The **xsmm-test** repository serves as meta project to test the
integration of a dense matrix implementation with
[libxsmm](https://github.com/hfp/libxsmm) support as custom scalar
type (CST) with the C++ linear algebra expression template libraries
[Blaze](https://bitbucket.org/blaze-lib/blaze) and
[Eigen](http://eigen.tuxfamily.org).

---

# Getting started

To check out the `xsmm-test` repository together with its submodules type

```code{bash}
git clone https://gitlab.com/mmoelle1/xsmm-test.git
cd xsmm-test
git submodule init
git submodule update
```

With version 1.6.5 of Git and later you can simply run:
```code{bash}
git clone --recursive https://gitlab.com/mmoelle1/xsmm-test.git
cd xsmm-test
```

The following commands assume that you are in the top-level directory of the 
`xsmm-test` checkout.

## Configure Blaze

Change into the `blaze` subdirectory and call CMake to generate the configuration
and optimization files.

```code{bash}
cd blaze
CC=gcc CXX=g++ cmake . -Bbuild
```

## Configure Eigen

Change into the `Eigen` subdirectory and call CMake to generate the configuration
and optimization files.

```code{bash}
cd eigen
CC=gcc CXX=g++ cmake . -Bbuild
```

## Configure libxsmm for use in header-only

Change into the `libxsmm` subdirectory and generate the file `libxsmm_source.h`,
which is required to use `libxsmm` in header-only mode.

```code{bash}
cd libxsmm
CC=gcc CXX=g++ FC=gfortran make header-only
```

---

# Running the Blaze tests

The `Blaze` backend can be tested using the `eigen_test.sh` shell script.
This script will compile and run the source file `eigen_test.cpp` with the 
selected set of configuration parameters. The `eigen_test.cpp` file tests the 
following operations:
- addition (A+A) and subtraction (A-A) of two matrices
- element-wise multiplication of a matrix with a scalar (c.*A and A.*c)
- element-wise division of a matrix by a scalar (A./c)
- matrix-matrix multiplication (C=A*B)

In all tests, the three possible matrices `A`, `B`, and `C` have the dimensions
`M x K`, `K x N`, and `M x N`, respectively. The test suite tests matrices with
intrinsic, i.e. `float` and `double`, scalar types and with the custom scalar
type `ScalarType<T,rows,cols>`, which represents a dense matrix with `rows` rows
and `cols` columns and data type `T`. For matrices `A`, `B`, and `C` the 
dimensions of the custom scalar types are `m x k`, `k x n`, and `m x n`,
respectively.

## Extensive testing of the Blaze backend

An extensive testing of the `Blaze` backend can be run as follows:

```code{bash}
cd libxsmm-testing/samples/eigen
BLAZE=1 sh eigen_test.sh
```

This will compile and run all possible combinations of matrix types 
- `Blaze::StaticMatrix`,
- `Blaze::DynamicMatrix`,
- `Blaze::HybridMatrix`, and 
- `Blaze::SparseMatrix`

for matrices `A`, `B`, and `C`. 

To test the ability to handle both square matrices and non-square matrices the
default setting for the dimension paramters are
- Test #1: `N=1`, `M=10`, `K=5`, `n=1`, `m=5`, and `k=3`
- Test #2: `N=5`, `M=5`, `K=5`, `n=5`, `m=5`, and `k=5`

The results of the entire build process are collected in the file `build.log`.
The outcome of runs is collected in file `run.log`. The names of the logfiles
can be overwritten by passing the variables `BUILDLOG` and `RUNLOG` to the
shell script
```code{bash}
BUILDLOG=mybuild.log RUNLOG=myrun.log ... sh eigen_test.sh
```

By default, the tests are run with 4 jobs in parallel. To change the number of
parallel jobs you can pass the `NPAR=x` variable to the shell script
```code{bash}
NPAR=8 ... sh eigen_test.sh
```

## Less extensive testing of the Blaze backend

A less extensive testing of the `Blaze` backend can be run as follows:

```code{bash}
cd libxsmm-testing/samples/eigen
N=1 M=10 K=5 n=1 m=5 k=3 BLAZE=1 sh eigen_test.sh
```

This will compile and run all possible combinations of matrix types for matrices
`A`, `B`, and `C` but only for the specified dimensions.

Likewise, the following commands will run the two different dimension configurations
for a fixed setting of matrix types:

```code{bash}
cd libxsmm-testing/samples/eigen
BLAZE=1 A=SPARSE B=STATIC C=DYNAMIC sh eigen_test.sh
```

## Individual testing of the Blaze backend

A specific test can be run by giving all parameters explicitly as follows:

```code{bash}
cd libxsmm-testing
N=1 M=10 K=5 n=1 m=5 k=3 BLAZE=1 A=SPARSE B=STATIC C=DYNAMIC sh eigen_test.sh
```
This will compile and run the following two tests:

```code{bash}
Duration: 2.25256 [N,M,K=1,10,5 n,m,k=1,5,3 LOOP A=SPARSE B=STATIC C=DYNAMIC BLAZE]
Duration: 6.31892 [N,M,K=1,10,5 n,m,k=1,5,3 XSMM A=SPARSE B=STATIC C=DYNAMIC BLAZE]
```

Here and above, `LOOP` and `XSMM` indicate that the specified tests have been
run using three nested for-loops for the matrix-matrix multiplication of the
scalar custom type and a call to `libxsmm`, respectively.

---

# Running the Eigen tests

The `Eigen` backend can be tested using the `eigen_test.sh` shell script with 
the same parameters as given above for the `Blaze` backend with `BLAZE=1`
replaced by `EIGEN=1`. All other settings remain unchanged.
