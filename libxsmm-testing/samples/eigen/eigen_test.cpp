#include <iostream>
#include <initializer_list>
#include <libxsmm_source.h>

#if defined(__BLAZE)
# include <blaze/Blaze.h>
#endif

#if defined(__EIGEN)
# include <Eigen/Eigen>
#endif

#include <ScalarType.hpp>

#ifndef __XSMM_m
#define __XSMM_m 3
#endif

#ifndef __XSMM_n
#define __XSMM_n 1
#endif

#ifndef __XSMM_k
#define __XSMM_k 3
#endif

template<typename T> using sA_t = fdbb::ScalarType<T,__XSMM_m,__XSMM_k>;
template<typename T> using sB_t = fdbb::ScalarType<T,__XSMM_k,__XSMM_n>;
template<typename T> using sC_t = fdbb::ScalarType<T,__XSMM_m,__XSMM_n>;
template<typename T> using sD_t = fdbb::ScalarType<T,__XSMM_m,__XSMM_k>;
template<typename T> using sE_t = fdbb::ScalarType<T,__XSMM_k,__XSMM_n>;

#ifndef __XSMM_M
#define __XSMM_M 1
#endif

#ifndef __XSMM_N
#define __XSMM_N 1
#endif

#ifndef __XSMM_K
#define __XSMM_K 1
#endif

#if defined(__BLAZE)

#if defined(__STATIC_A)
template<typename T> using A_s = blaze::StaticMatrix<T,__XSMM_M,__XSMM_K>;
template<typename T> using A_t = blaze::StaticMatrix<sA_t<T>,__XSMM_M,__XSMM_K>;
#elif defined(__DYNAMIC_A)
template<typename T> using A_s = blaze::DynamicMatrix<T>;
template<typename T> using A_t = blaze::DynamicMatrix<sA_t<T> >;
#elif defined(__HYBRID_A)
template<typename T> using A_s = blaze::HybridMatrix<T,__XSMM_M,__XSMM_K>;
template<typename T> using A_t = blaze::HybridMatrix<sA_t<T>,__XSMM_M,__XSMM_K>;
#elif defined(__SPARSE_A)
template<typename T> using A_s = blaze::CompressedMatrix<T>;
template<typename T> using A_t = blaze::CompressedMatrix<sA_t<T> >;
#else
#error "One of __STATIC_A, __DYNAMIC_A, __HYBRID_A, or __SPARSE_A must be defined."
#endif

#if defined(__STATIC_B)
template<typename T> using B_s = blaze::StaticMatrix<T,__XSMM_K,__XSMM_N>;
template<typename T> using B_t = blaze::StaticMatrix<sB_t<T>,__XSMM_K,__XSMM_N>;
#elif defined(__DYNAMIC_B)
template<typename T> using B_s = blaze::DynamicMatrix<T>;
template<typename T> using B_t = blaze::DynamicMatrix<sB_t<T> >;
#elif defined(__HYBRID_B)
template<typename T> using B_s = blaze::HybridMatrix<T,__XSMM_K,__XSMM_N>;
template<typename T> using B_t = blaze::HybridMatrix<sB_t<T>,__XSMM_K,__XSMM_N>;
#elif defined(__SPARSE_B)
template<typename T> using B_s = blaze::CompressedMatrix<T>;
template<typename T> using B_t = blaze::CompressedMatrix<sB_t<T> >;
#else
#error "One of __STATIC_B, __DYNAMIC_B, __HYBRID_B, or __SPARSE_B must be defined."
#endif

#if defined(__STATIC_C)
template<typename T> using C_s = blaze::StaticMatrix<T,__XSMM_M,__XSMM_N>;
template<typename T> using D_s = blaze::StaticMatrix<T,__XSMM_M,__XSMM_N>;
template<typename T> using E_s = blaze::StaticMatrix<T,__XSMM_M,__XSMM_N>;

template<typename T> using C_t = blaze::StaticMatrix<sC_t<T>,__XSMM_M,__XSMM_N>;
template<typename T> using D_t = blaze::StaticMatrix<sD_t<T>,__XSMM_M,__XSMM_N>;
template<typename T> using E_t = blaze::StaticMatrix<sE_t<T>,__XSMM_M,__XSMM_N>;
#elif defined(__DYNAMIC_C)
template<typename T> using C_s = blaze::DynamicMatrix<T>;
template<typename T> using D_s = blaze::DynamicMatrix<T>;
template<typename T> using E_s = blaze::DynamicMatrix<T>;

template<typename T> using C_t = blaze::DynamicMatrix<sC_t<T> >;
template<typename T> using D_t = blaze::DynamicMatrix<sD_t<T> >;
template<typename T> using E_t = blaze::DynamicMatrix<sE_t<T> >;
#elif defined(__HYBRID_C)
template<typename T> using C_s = blaze::HybridMatrix<T,__XSMM_M,__XSMM_N>;
template<typename T> using D_s = blaze::HybridMatrix<T,__XSMM_M,__XSMM_N>;
template<typename T> using E_s = blaze::HybridMatrix<T,__XSMM_M,__XSMM_N>;

template<typename T> using C_t = blaze::HybridMatrix<sC_t<T>,__XSMM_M,__XSMM_N>;
template<typename T> using D_t = blaze::HybridMatrix<sD_t<T>,__XSMM_M,__XSMM_N>;
template<typename T> using E_t = blaze::HybridMatrix<sE_t<T>,__XSMM_M,__XSMM_N>;
#elif defined(__SPARSE_C)
template<typename T> using C_s = blaze::CompressedMatrix<T>;
template<typename T> using D_s = blaze::CompressedMatrix<T>;
template<typename T> using E_s = blaze::CompressedMatrix<T>;

template<typename T> using C_t = blaze::CompressedMatrix<sC_t<T> >;
template<typename T> using D_t = blaze::CompressedMatrix<sD_t<T> >;
template<typename T> using E_t = blaze::CompressedMatrix<sE_t<T> >;
#else
#error "One of __STATIC_C, __DYNAMIC_C, __HYBRID_C, or __SPARSE_C must be defined."
#endif

#endif // defined(__BLAZE)


#if defined(__EIGEN)

#if defined(__STATIC_A)
template<typename T> using A_s = Eigen::Matrix<T,__XSMM_M,__XSMM_K>;
template<typename T> using A_t = Eigen::Matrix<sA_t<T>,__XSMM_M,__XSMM_K>;
#elif defined(__DYNAMIC_A)
template<typename T> using A_s = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;
template<typename T> using A_t = Eigen::Matrix<sA_t<T>,Eigen::Dynamic,Eigen::Dynamic>;
#elif defined(__SPARSE_A)
template<typename T> using A_s = Eigen::SparseMatrix<T>;
template<typename T> using A_t = Eigen::SparseMatrix<sA_t<T>>;
#else
#error "One of __STATIC_A, __DYNAMIC_A, or __SPARSE_A must be defined."
#endif

#if defined(__STATIC_B)
template<typename T> using B_s = Eigen::Matrix<T,__XSMM_K,__XSMM_N>;
template<typename T> using B_t = Eigen::Matrix<sB_t<T>,__XSMM_K,__XSMM_N>;
#elif defined(__DYNAMIC_B)
template<typename T> using B_s = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;
template<typename T> using B_t = Eigen::Matrix<sB_t<T>,Eigen::Dynamic,Eigen::Dynamic>;
#elif defined(__SPARSE_B)
template<typename T> using B_s = Eigen::SparseMatrix<T>;
template<typename T> using B_t = Eigen::SparseMatrix<sB_t<T>>;
#else
#error "One of __STATIC_B, __DYNAMIC_B, or __SPARSE_B must be defined."
#endif

#if defined(__STATIC_C)
template<typename T> using C_s = Eigen::Matrix<T,__XSMM_M,__XSMM_N>;
template<typename T> using D_s = Eigen::Matrix<T,__XSMM_M,__XSMM_N>;
template<typename T> using E_s = Eigen::Matrix<T,__XSMM_M,__XSMM_N>;

template<typename T> using C_t = Eigen::Matrix<sC_t<T>,__XSMM_M,__XSMM_N>;
template<typename T> using D_t = Eigen::Matrix<sD_t<T>,__XSMM_M,__XSMM_N>;
template<typename T> using E_t = Eigen::Matrix<sE_t<T>,__XSMM_M,__XSMM_N>;
#elif defined(__DYNAMIC_C)
template<typename T> using C_s = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;
template<typename T> using D_s = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;
template<typename T> using E_s = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;

template<typename T> using C_t = Eigen::Matrix<sC_t<T>,Eigen::Dynamic,Eigen::Dynamic>;
template<typename T> using D_t = Eigen::Matrix<sD_t<T>,Eigen::Dynamic,Eigen::Dynamic>;
template<typename T> using E_t = Eigen::Matrix<sE_t<T>,Eigen::Dynamic,Eigen::Dynamic>;
#elif defined(__SPARSE_C)
template<typename T> using C_s = Eigen::SparseMatrix<T>;
template<typename T> using D_s = Eigen::SparseMatrix<T>;
template<typename T> using E_s = Eigen::SparseMatrix<T>;

template<typename T> using C_t = Eigen::SparseMatrix<sC_t<T>>;
template<typename T> using D_t = Eigen::SparseMatrix<sD_t<T>>;
template<typename T> using E_t = Eigen::SparseMatrix<sE_t<T>>;
#else
#error "One of __STATIC_C, __DYNAMIC_C, or __SPARSE_C must be defined."
#endif

#endif // defined(__EIGEN)

int main()
{    
#if defined(__XSMM)
    // initialize LIBXSMM
    libxsmm_init();
    
    fdbb::xsmm_set<float,__XSMM_n,__XSMM_m,__XSMM_k,
                   LIBXSMM_GEMM_FLAG_NONE, LIBXSMM_PREFETCH_AUTO>::init();
    fdbb::xsmm_set<double,__XSMM_n,__XSMM_m,__XSMM_k,
                   LIBXSMM_GEMM_FLAG_NONE, LIBXSMM_PREFETCH_AUTO>::init();
    fdbb::xsmm_add<float,__XSMM_n,__XSMM_m,__XSMM_k,
                   LIBXSMM_GEMM_FLAG_NONE, LIBXSMM_PREFETCH_AUTO>::init();
    fdbb::xsmm_add<double,__XSMM_n,__XSMM_m,__XSMM_k,
                   LIBXSMM_GEMM_FLAG_NONE, LIBXSMM_PREFETCH_AUTO>::init();
#endif

    const unsigned long long start = libxsmm_timer_tick();
    
    // Extensive tests of the custom scalar type per se

    // ADD
    { sA_t<float>  A = sA_t<float>(1)  + sA_t<float>(1);  }
    { sA_t<double> A = sA_t<float>(1)  + sA_t<double>(1); }
    { sA_t<double> A = sA_t<double>(1) + sA_t<float>(1);  }
    { sA_t<double> A = sA_t<double>(1) + sA_t<double>(1); }

    { sA_t<float>  A = sA_t<float>(1)  + float(1);  }
    { sA_t<double> A = sA_t<float>(1)  + double(1); }
    { sA_t<double> A = sA_t<double>(1) + float(1);  }
    { sA_t<double> A = sA_t<double>(1) + double(1); }

    { sA_t<float>  A = float(1)  + sA_t<float>(1);  }
    { sA_t<double> A = float(1)  + sA_t<double>(1); }
    { sA_t<double> A = double(1) + sA_t<float>(1);  }
    { sA_t<double> A = double(1) + sA_t<double>(1); }

    // SUB
    { sA_t<float>  A = sA_t<float>(1)  - sA_t<float>(1);  }
    { sA_t<double> A = sA_t<float>(1)  - sA_t<double>(1); }
    { sA_t<double> A = sA_t<double>(1) - sA_t<float>(1);  }
    { sA_t<double> A = sA_t<double>(1) - sA_t<double>(1); }

    { sA_t<float>  A = sA_t<float>(1)  - float(1);  }
    { sA_t<double> A = sA_t<float>(1)  - double(1); }
    { sA_t<double> A = sA_t<double>(1) - float(1);  }
    { sA_t<double> A = sA_t<double>(1) - double(1); }

    { sA_t<float>  A = float(1)  - sA_t<float>(1);  }
    { sA_t<double> A = float(1)  - sA_t<double>(1); }
    { sA_t<double> A = double(1) - sA_t<float>(1);  }
    { sA_t<double> A = double(1) - sA_t<double>(1); }

    // MUL
    { sA_t<float>  A = sA_t<float>(1)  * float(1);  }
    { sA_t<double> A = sA_t<float>(1)  * double(1); }
    { sA_t<double> A = sA_t<double>(1) * float(1);  }
    { sA_t<double> A = sA_t<double>(1) * double(1); }

    { sA_t<float>  A = float(1)  * sA_t<float>(1);  }
    { sA_t<double> A = float(1)  * sA_t<double>(1); }
    { sA_t<double> A = double(1) * sA_t<float>(1);  }
    { sA_t<double> A = double(1) * sA_t<double>(1); }

    // DIV
    { sA_t<float>  A = sA_t<float>(1)  / float(1);  }
    { sA_t<double> A = sA_t<float>(1)  / double(1); }
    { sA_t<double> A = sA_t<double>(1) / float(1);  }
    { sA_t<double> A = sA_t<double>(1) / double(1); }

    // ADD-assign
    { sA_t<float>  A(1); A += sA_t<float>(1);  }
    { sA_t<double> A(1); A += sA_t<double>(1); }
    { sA_t<double> A(1); A += sA_t<float>(1);  }
    { sA_t<double> A(1); A += sA_t<double>(1); }

    { sA_t<float>  A(1); A += float(1);  }
    { sA_t<double> A(1); A += double(1); }
    { sA_t<double> A(1); A += float(1);  }
    { sA_t<double> A(1); A += double(1); }

    // SUB-assign
    { sA_t<float>  A(1); A -= sA_t<float>(1);  }
    { sA_t<double> A(1); A -= sA_t<double>(1); }
    { sA_t<double> A(1); A -= sA_t<float>(1);  }
    { sA_t<double> A(1); A -= sA_t<double>(1); }

    { sA_t<float>  A(1); A -= float(1);  }
    { sA_t<double> A(1); A -= double(1); }
    { sA_t<double> A(1); A -= float(1);  }
    { sA_t<double> A(1); A -= double(1); }

    // MUL-assign
#if __XSMM_m == __XSMM_k
    { sA_t<float>  A(1); A *= sA_t<float>(1);  }
    { sA_t<double> A(1); A *= sA_t<double>(1); }
    { sA_t<double> A(1); A *= sA_t<float>(1);  }
    { sA_t<double> A(1); A *= sA_t<double>(1); }
#endif
    
    { sA_t<float>  A(1); A *= float(1);  }
    { sA_t<double> A(1); A *= double(1); }
    { sA_t<double> A(1); A *= float(1);  }
    { sA_t<double> A(1); A *= double(1); }

    // DIV-assign
    { sA_t<float>  A(1); A /= float(1);  }
    { sA_t<double> A(1); A /= double(1); }
    { sA_t<double> A(1); A /= float(1);  }
    { sA_t<double> A(1); A /= double(1); }

    // MAT-MAT-MUL
    { sC_t<float>  C = sA_t<float>(1)  * sB_t<float>(1);  }
    { sC_t<double> C = sA_t<float>(1)  * sB_t<double>(1); }
    { sC_t<double> C = sA_t<double>(1) * sB_t<float>(1);  }
    { sC_t<double> C = sA_t<double>(1) * sB_t<double>(1); }

    // Extensive tests of the custom scalar type as scalar type in Blaze/Eigen
    
#if defined(__STATIC_A)
    A_s<float>  Af_s;
    A_s<double> Ad_s;
    A_t<float>  Af_t;
    A_t<double> Ad_t;
#else
    A_s<float>  Af_s(__XSMM_M,__XSMM_K);
    A_s<double> Ad_s(__XSMM_M,__XSMM_K);
    A_t<float>  Af_t(__XSMM_M,__XSMM_K);
    A_t<double> Ad_t(__XSMM_M,__XSMM_K);
#endif

#if defined(__STATIC_B)
    B_s<float>  Bf_s;
    B_s<double> Bd_s;
    B_t<float>  Bf_t;
    B_t<double> Bd_t;
#else
    B_s<float>  Bf_s(__XSMM_K,__XSMM_N);
    B_s<double> Bd_s(__XSMM_K,__XSMM_N);
    B_t<float>  Bf_t(__XSMM_K,__XSMM_N);
    B_t<double> Bd_t(__XSMM_K,__XSMM_N);
#endif
    
#if defined(__STATIC_C)
    C_s<float> Cf_s;
    D_s<float> Df_s;
    E_s<float> Ef_s;

    C_s<double> Cd_s;
    D_s<double> Dd_s;
    E_s<double> Ed_s;

    C_t<float> Cf_t, Cf_check_t;
    D_t<float> Df_t, Df_check_t;
    E_t<float> Ef_t, Ef_check_t;

    C_t<double> Cd_t, Cd_check_t;
    D_t<double> Dd_t, Dd_check_t;
    E_t<double> Ed_t, Ed_check_t;
#else
    C_s<float> Cf_s(__XSMM_M,__XSMM_N);
    D_s<float> Df_s(__XSMM_M,__XSMM_N);
    E_s<float> Ef_s(__XSMM_M,__XSMM_N);

    C_s<double> Cd_s(__XSMM_M,__XSMM_N);
    D_s<double> Dd_s(__XSMM_M,__XSMM_N);
    E_s<double> Ed_s(__XSMM_M,__XSMM_N);

    C_t<float> Cf_t(__XSMM_M,__XSMM_N), Cf_check_t(__XSMM_M,__XSMM_N);
    D_t<float> Df_t(__XSMM_M,__XSMM_N), Df_check_t(__XSMM_M,__XSMM_N);
    E_t<float> Ef_t(__XSMM_M,__XSMM_N), Ef_check_t(__XSMM_M,__XSMM_N);

    C_t<double> Cd_t(__XSMM_M,__XSMM_N), Cd_check_t(__XSMM_M,__XSMM_N);
    D_t<double> Dd_t(__XSMM_M,__XSMM_N), Dd_check_t(__XSMM_M,__XSMM_N);
    E_t<double> Ed_t(__XSMM_M,__XSMM_N), Ed_check_t(__XSMM_M,__XSMM_N);
#endif
    
#if defined(__SPARSE_A) && defined(__EIGEN)
    for (int i=0; i<__XSMM_M; i++) {
        for (int j=0; j<__XSMM_K; j++) {
            Af_s.coeffRef(i,j) = float(100*i+j+1);
            Ad_s.coeffRef(i,j) = double(100*i+j+1);
            Af_t.coeffRef(i,j) = float(100*i+j+1)  * sA_t<float>(1);
            Ad_t.coeffRef(i,j) = double(100*i+j+1) * sA_t<double>(1);
        }
    }
#else
    for (int i=0; i<__XSMM_M; i++) {
        for (int j=0; j<__XSMM_K; j++) {
            Af_s(i,j) = float(100*i+j+1);
            Ad_s(i,j) = double(100*i+j+1);
            Af_t(i,j) = float(100*i+j+1)  * sA_t<float>(1);
            Ad_t(i,j) = double(100*i+j+1) * sA_t<double>(1);
        }
    }    
#endif
        
    // ADD
#if defined(__BLAZE)
    { Af_s = Af_s + Af_s; }    
    { Ad_s = Af_s + Ad_s; }
    { Ad_s = Ad_s + Af_s; }
    { Ad_s = Ad_s + Ad_s; }
        
    { Af_t = Af_t + Af_t; }
    { Ad_t = Af_t + Ad_t; }
    { Ad_t = Ad_t + Af_t; }
    { Ad_t = Ad_t + Ad_t; }
    
    { Af_t = Af_t + Af_s; }
    { Ad_t = Af_t + Ad_s; }
    { Ad_t = Ad_t + Af_s; }
    { Ad_t = Ad_t + Ad_s; }

    { Af_t = Af_s + Af_t; }
    { Ad_t = Af_s + Ad_t; }
    { Ad_t = Ad_s + Af_t; }
    { Ad_t = Ad_s + Ad_t; }
#endif

    // ADD
#if defined(__EIGEN)
    { Af_s = Af_s + Af_s; }
    { Ad_s = Af_s.cast<double>() + Ad_s; }
    { Ad_s = Ad_s + Af_s.cast<double>(); }
    { Ad_s = Ad_s + Ad_s; }
        
    { Af_t = Af_t + Af_t; }
    { Ad_t = Af_t.cast<sA_t<double>>() + Ad_t; }
    { Ad_t = Ad_t + Af_t.cast<sA_t<double>>(); }
    { Ad_t = Ad_t + Ad_t; }

#if !defined(__SPARSE_A)
    { Af_t = Af_t + Af_s; }
    { Ad_t = Af_t.cast<sA_t<double>>() + Ad_s; }
    { Ad_t = Ad_t + Af_s.cast<sA_t<double>>(); }
    { Ad_t = Ad_t + Ad_s; }

    { Af_t = Af_s + Af_t; }
    { Ad_t = Af_s.cast<sA_t<double>>() + Ad_t; }
    { Ad_t = Ad_s + Af_t.cast<sA_t<double>>(); }
    { Ad_t = Ad_s + Ad_t; }
#endif
#endif
    
    // SUB
#if defined(__BLAZE)
    { Af_s = Af_s - Af_s; }
    { Ad_s = Af_s - Ad_s; }
    { Ad_s = Ad_s - Af_s; }
    { Ad_s = Ad_s - Ad_s; }
    
    { Af_t = Af_t - Af_t; }
    { Ad_t = Af_t - Ad_t; }
    { Ad_t = Ad_t - Af_t; }
    { Ad_t = Ad_t - Ad_t; }

    { Af_t = Af_t - Af_s; }
    { Ad_t = Af_t - Ad_s; }
    { Ad_t = Ad_t - Af_s; }
    { Ad_t = Ad_t - Ad_s; }

    { Af_t = Af_s - Af_t; }
    { Ad_t = Af_s - Ad_t; }
    { Ad_t = Ad_s - Af_t; }
    { Ad_t = Ad_s - Ad_t; }
#endif

    // SUB
#if defined(__EIGEN)
    { Af_s = Af_s - Af_s; }
    { Ad_s = Af_s.cast<double>() - Ad_s; }
    { Ad_s = Ad_s - Af_s.cast<double>(); }
    { Ad_s = Ad_s - Ad_s; }
    
    { Af_t = Af_t - Af_t; }
    { Ad_t = Af_t.cast<sA_t<double>>() - Ad_t; }
    { Ad_t = Ad_t - Af_t.cast<sA_t<double>>(); }
    { Ad_t = Ad_t - Ad_t; }

#if !defined(__SPARSE_A)
    { Af_t = Af_t - Af_s; }
    { Ad_t = Af_t.cast<sA_t<double>>() - Ad_s; }
    { Ad_t = Ad_t - Af_s.cast<sA_t<double>>(); }
    { Ad_t = Ad_t - Ad_s; }

    { Af_t = Af_s - Af_t; }
    { Ad_t = Af_s.cast<sA_t<double>>() - Ad_t; }
    { Ad_t = Ad_s - Af_t.cast<sA_t<double>>(); }
    { Ad_t = Ad_s - Ad_t; }
#endif
#endif
    
    // MUL
#if defined(__BLAZE)
    { Af_s = 2.0f * Af_s; }
    { Ad_s = 2.0  * Af_s; }
    { Ad_s = 2.0f * Ad_s; }
    { Ad_s = 2.0  * Ad_s; }

    { Af_s = Af_s * 2.0f; }
    { Ad_s = Af_s * 2.0;  }
    { Ad_s = Ad_s * 2.0f; }
    { Ad_s = Ad_s * 2.0;  }
    
    { Af_t = 2.0f * Af_t; }
    { Ad_t = 2.0  * Af_t; }
    { Ad_t = 2.0f * Ad_t; }
    { Ad_t = 2.0  * Ad_t; }
    
    { Af_t = Af_t * 2.0f; }
    { Ad_t = Af_t * 2.0;  }
    { Ad_t = Ad_t * 2.0f; }
    { Ad_t = Ad_t * 2.0;  }
#endif

    // MUL
#if defined(__EIGEN)
    { Af_s = 2.0f * Af_s; }
    /*{ Ad_s = 2.0  * Af_s; }*/
    /*{ Ad_s = 2.0f * Ad_s; }*/
    { Ad_s = 2.0  * Ad_s; }

    { Af_s = Af_s * 2.0f; }
    /*{ Ad_s = Af_s * 2.0;  }*/
    /*{ Ad_s = Ad_s * 2.0f; }*/
    { Ad_s = Ad_s * 2.0;  }
    
    { Af_t = 2.0f * Af_t; }
    /*{ Ad_t = 2.0  * Af_t; }*/
    /*{ Ad_t = 2.0f * Ad_t; }*/
    { Ad_t = 2.0  * Ad_t; }
    
    { Af_t = Af_t * 2.0f; }
    /*{ Ad_t = Af_t * 2.0;  }*/
    /*{ Ad_t = Ad_t * 2.0f; }*/
    { Ad_t = Ad_t * 2.0;  }
#endif

    // DIV
#if defined(__BLAZE)
    { Af_s = Af_s / 2.0f; }
    { Ad_s = Af_s / 2.0;  }
    { Ad_s = Ad_s / 2.0f; }
    { Ad_s = Ad_s / 2.0;  }
    
    { Af_t = Af_t / 2.0f; }
    { Ad_t = Af_t / 2.0;  }
    { Ad_t = Ad_t / 2.0f; }
    { Ad_t = Ad_t / 2.0;  }
#endif

    // DIV
#if defined(__EIGEN)
    { Af_s = Af_s / 2.0f; }
    /*{ Ad_s = Af_s / 2.0;  }*/
    /*{ Ad_s = Ad_s / 2.0f; }*/
    { Ad_s = Ad_s / 2.0;  }
    
    { Af_t = Af_t / 2.0f; }
    /*{ Ad_t = Af_t / 2.0;  }*/
    /*{ Ad_t = Ad_t / 2.0f; }*/
    { Ad_t = Ad_t / 2.0;  }
#endif
    
#if defined(__SPARSE_A) && defined(__EIGEN)
    for (int i=0; i<__XSMM_M; i++) {
        for (int j=0; j<__XSMM_K; j++) {
            Af_s.coeffRef(i,j) = float(100*i+j+1);
            Ad_s.coeffRef(i,j) = double(100*i+j+1);
            Af_t.coeffRef(i,j) = float(100*i+j+1)  * sA_t<float>(1);
            Ad_t.coeffRef(i,j) = double(100*i+j+1) * sA_t<double>(1);
        }
    }
#else
    for (int i=0; i<__XSMM_M; i++) {
        for (int j=0; j<__XSMM_K; j++) {
            Af_s(i,j) = float(100*i+j+1);
            Ad_s(i,j) = double(100*i+j+1);
            Af_t(i,j) = float(100*i+j+1)  * sA_t<float>(1);
            Ad_t(i,j) = double(100*i+j+1) * sA_t<double>(1);
        }
    }
#endif

#if defined(__SPARSE_B) && defined(__EIGEN)
    for (int i=0; i<__XSMM_K; i++) {
        for (int j=0; j<__XSMM_N; j++) {
            Bf_s.coeffRef(i,j) = float(100*j+i+1);
            Bd_s.coeffRef(i,j) = double(100*j+i+1);
            Bf_t.coeffRef(i,j) = float(100*j+i+1)  * sB_t<float>(1);
            Bd_t.coeffRef(i,j) = double(100*j+i+1) * sB_t<double>(1);
        }
    }    
#else
    for (int i=0; i<__XSMM_K; i++) {
        for (int j=0; j<__XSMM_N; j++) {
            Bf_s(i,j) = float(100*j+i+1);
            Bd_s(i,j) = double(100*j+i+1);
            Bf_t(i,j) = float(100*j+i+1)  * sB_t<float>(1);
            Bd_t(i,j) = double(100*j+i+1) * sB_t<double>(1);
        }
    }
#endif

#if defined(__SPARSE_C) && defined(__EIGEN)
    for (int i=0; i<__XSMM_M; i++) {
        for (int j=0; j<__XSMM_N; j++) {

            Cf_check_t.coeffRef(i,j) = 0;
            Cd_check_t.coeffRef(i,j) = 0;
            
            for (int k=0; k<__XSMM_K; k++) {
                Cf_check_t.coeffRef(i,j) += __XSMM_k * float(100*i+k+1) * float(100*j+k+1)  * sC_t<float>(1);
                Cd_check_t.coeffRef(i,j) += __XSMM_k * double(100*i+k+1) * double(100*j+k+1) * sC_t<double>(1);
            }
            
            Df_check_t.coeffRef(i,j) = 0;
            Dd_check_t.coeffRef(i,j) = 0;
            
            for (int k=0; k<__XSMM_K; k++) {
                Df_check_t.coeffRef(i,j) += float(100*i+k+1) * float(100*j+k+1)  * sD_t<float>(1);
                Dd_check_t.coeffRef(i,j) += double(100*i+k+1) * double(100*j+k+1) * sD_t<double>(1);
            }

            Ef_check_t.coeffRef(i,j) = 0;
            Ed_check_t.coeffRef(i,j) = 0;
            
            for (int k=0; k<__XSMM_K; k++) {
                Ef_check_t.coeffRef(i,j) += float(100*i+k+1) * float(100*j+k+1)  * sE_t<float>(1);
                Ed_check_t.coeffRef(i,j) += double(100*i+k+1) * double(100*j+k+1) * sE_t<double>(1);
            }
        }
    }    
#else
    for (int i=0; i<__XSMM_M; i++) {
        for (int j=0; j<__XSMM_N; j++) {

            Cf_check_t(i,j) = 0;
            Cd_check_t(i,j) = 0;

            for (int k=0; k<__XSMM_K; k++) {
                Cf_check_t(i,j) += __XSMM_k * float(100*i+k+1) * float(100*j+k+1)  * sC_t<float>(1);
                Cd_check_t(i,j) += __XSMM_k * double(100*i+k+1) * double(100*j+k+1) * sC_t<double>(1);
            }

            Df_check_t(i,j) = 0;
            Dd_check_t(i,j) = 0;

            for (int k=0; k<__XSMM_K; k++) {
                Df_check_t(i,j) += float(100*i+k+1) * float(100*j+k+1)  * sD_t<float>(1);
                Dd_check_t(i,j) += double(100*i+k+1) * double(100*j+k+1) * sD_t<double>(1);
            }

            Ef_check_t(i,j) = 0;
            Ed_check_t(i,j) = 0;

            for (int k=0; k<__XSMM_K; k++) {
                Ef_check_t(i,j) += float(100*i+k+1) * float(100*j+k+1)  * sE_t<float>(1);
                Ed_check_t(i,j) += double(100*i+k+1) * double(100*j+k+1) * sE_t<double>(1);
            }
        }
    }
#endif
    
    // MAT-MAT-MUL
#if defined(__BLAZE)
    { Cf_s = Af_s * Bf_s; }
    { Cd_s = Af_s * Bd_s; }
    { Cd_s = Ad_s * Bf_s; }
    { Cd_s = Ad_s * Bd_s; }

    {
        Cf_t = Af_t * Bf_t;
    }
    {
        Cd_t = Af_t * Bd_t;
    }
    {
        Cd_t = Ad_t * Bf_t;
    }
    {
        Cd_t = Ad_t * Bd_t;
    }

    {
        Df_t = Af_t * Bf_s;
    }
    {
        Dd_t = Af_t * Bd_s;
    }
    {
        Dd_t = Ad_t * Bf_s;
    }
    {
        Dd_t = Ad_t * Bd_s;
    }

    {
        Ef_t = Af_s * Bf_t;
    }
    {
        Ed_t = Af_s * Bd_t;
    }
    {
        Ed_t = Ad_s * Bf_t;
    }
    {
        Ed_t = Ad_s * Bd_t;
    }
#endif

    // MAT-MAT-MUL
#if defined(__EIGEN)
    { Cf_s = Af_s * Bf_s; }
    { Cd_s = Af_s.cast<double>() * Bd_s; }
    { Cd_s = Ad_s * Bf_s.cast<double>(); }
    { Cd_s = Ad_s * Bd_s; }

    {
        Cf_t = Af_t * Bf_t;
        if ((Cf_t-Cf_check_t).norm() != sC_t<float>(0)) exit(1);
    }
    {
        Cd_t = Af_t.cast<sA_t<double>>() * Bd_t;
        if ((Cd_t-Cd_check_t).norm() != sC_t<double>(0)) exit(1);
    }
    {
        Cd_t = Ad_t * Bf_t.cast<sB_t<double>>();
        if ((Cd_t-Cd_check_t).norm() != sC_t<double>(0)) exit(1);
    }
    {
        Cd_t = Ad_t * Bd_t;
        if ((Cd_t-Cd_check_t).norm() != sC_t<double>(0)) exit(1);
    }

#if !defined(__SPARSE)
    {
        Df_t = Af_t * Bf_s;
        if ((Df_t-Df_check_t).norm() != sD_t<float>(0)) exit(1);
    }
    {
        Dd_t = Af_t.cast<sA_t<double>>() * Bd_s;
        if ((Dd_t-Dd_check_t).norm() != sD_t<double>(0)) exit(1);
    }
    {
        Dd_t = Ad_t * Bf_s.cast<double>();
        if ((Dd_t-Dd_check_t).norm() != sD_t<double>(0)) exit(1);
    }
    {
        Dd_t = Ad_t * Bd_s;
        if ((Dd_t-Dd_check_t).norm() != sD_t<double>(0)) exit(1);
    }

    {
        Ef_t = Af_s * Bf_t;
        if ((Ef_t-Ef_check_t).norm() != sE_t<float>(0)) exit(1);
    }
    {
        Ed_t = Af_s.cast<double>() * Bd_t;
        if ((Ed_t-Ed_check_t).norm() != sE_t<double>(0)) exit(1);
    }
    {
        Ed_t = Ad_s * Bf_t.cast<sB_t<double>>();
        if ((Ed_t-Ed_check_t).norm() != sE_t<double>(0)) exit(1);
    }
    {
        Ed_t = Ad_s * Bd_t;
        if ((Ed_t-Ed_check_t).norm() != sE_t<double>(0)) exit(1);
    }
#endif
#endif
    
    const unsigned long long end = libxsmm_timer_tick();
    const double duration = libxsmm_timer_duration(start, end);
    
    std::cout << "Duration: " << 1000.0 * duration
              << " [N,M,K=" << __XSMM_N << "," << __XSMM_M << "," << __XSMM_K
              <<  " n,m,k=" << __XSMM_n << "," << __XSMM_m << "," << __XSMM_k
#if defined(__XSMM)
              <<  " XSMM"
#else
              <<  " LOOP"
#endif
#if defined(__DYNAMIC_A)
              <<  " A=DYNAMIC"
#endif
#if defined(__HYBRID_A)
              <<  " A=HYBRID"
#endif
#if defined(__SPARSE_A)
              <<  " A=SPARSE"
#endif
#if defined(__STATIC_A)
              <<  " A=STATIC"
#endif
#if defined(__DYNAMIC_B)
              <<  " B=DYNAMIC"
#endif
#if defined(__HYBRID_B)
              <<  " B=HYBRID"
#endif
#if defined(__SPARSE_B)
              <<  " B=SPARSE"
#endif
#if defined(__STATIC_B)
              <<  " B=STATIC"
#endif
#if defined(__DYNAMIC_C)
              <<  " C=DYNAMIC"
#endif
#if defined(__HYBRID_C)
              <<  " C=HYBRID"
#endif
#if defined(__SPARSE_C)
              <<  " C=SPARSE"
#endif
#if defined(__STATIC_C)
              <<  " C=STATIC"
#endif
#if defined(__BLAZE)
              <<  " BLAZE"
#endif
#if defined(__EIGEN)
              <<  " EIGEN"
#endif
              << "]" << std::endl;
    
#if defined(__XSMM)
    // finalize LIBXSMM
    libxsmm_finalize();
#endif
    
    return 0;
}
