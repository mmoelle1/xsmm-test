#ifndef SCALAR_TYPE_HPP
#define SCALAR_TYPE_HPP

#include <exception>

namespace fdbb {

//------------------------------------------------------------------------------
// LIBXSMM
//------------------------------------------------------------------------------

/**
 * Libxsmm matrix-matrix multipier
 */
template<typename T, int m, int n, int k,
         int flags    = LIBXSMM_GEMM_FLAG_NONE,
         int prefetch = LIBXSMM_PREFETCH_NONE>
struct xsmm_gen
{
    static libxsmm_mmfunction<T> xmm;
    static void init(const T alpha, const T beta);
};

// Initialization of matrix-matrix multiply
template<typename T, int m, int n, int k, int flags, int prefetch>
libxsmm_mmfunction<T> xsmm_gen<T,m,n,k,flags,prefetch>::xmm;

// Initialization of matrix-matrix multiply plus y
template<typename T, int m, int n, int k, int flags, int prefetch>
void xsmm_gen<T,m,n,k,flags,prefetch>::init(const T alpha, const T beta)
{
    xmm = libxsmm_mmfunction<T>(flags, m, n, k, alpha, beta, prefetch);
}

/**
 * Libxsmm matrix-matrix multipier
 */
template<typename T, int m, int n, int k,
         int flags    = LIBXSMM_GEMM_FLAG_NONE,
         int prefetch = LIBXSMM_PREFETCH_NONE>
struct xsmm_set
{
    static libxsmm_mmfunction<T> xmm;
    static void init();
};

// Initialization of matrix-matrix multiply
template<typename T, int m, int n, int k, int flags, int prefetch>
libxsmm_mmfunction<T> xsmm_set<T,m,n,k,flags,prefetch>::xmm;

// Initialization of matrix-matrix multiply plus y
template<typename T, int m, int n, int k, int flags, int prefetch>
void xsmm_set<T,m,n,k,flags,prefetch>::init()
{
    xmm = libxsmm_mmfunction<T>(flags, m, n, k,
                                1/*alpha*/, 0/*beta*/, prefetch);
}

/**
 * Libxsmm matrix-matrix multipier
 */
template<typename T, int m, int n, int k,
         int flags    = LIBXSMM_GEMM_FLAG_NONE,
         int prefetch = LIBXSMM_PREFETCH_NONE>
struct xsmm_add
{
    static libxsmm_mmfunction<T> xmm;
    static void init();
};

// Initialization of matrix-matrix multiply
template<typename T, int m, int n, int k, int flags, int prefetch>
libxsmm_mmfunction<T> xsmm_add<T,m,n,k,flags,prefetch>::xmm;

// Initialization of matrix-matrix multiply plus y
template<typename T, int m, int n, int k, int flags, int prefetch>
void xsmm_add<T,m,n,k,flags,prefetch>::init()
{
    xmm = libxsmm_mmfunction<T>(flags, m, n, k,
                                1/*alpha*/, 1/*beta*/, prefetch);
}

//------------------------------------------------------------------------------
// CUSTOM SCALAR TYPE
//------------------------------------------------------------------------------

/**
 * Custom scalar base type
 */
struct ScalarTypeBase
{
};

/**
 * Custom scalar type
 */
template<typename T, int rows, int cols>
struct ScalarType : public ScalarTypeBase
{
    /**
       Matrix data
    */
    T data[rows][cols];

#if defined(__BLAZE)
    /**
       Element type (required by Blaze)
    */
    using ElementType = T;
#endif

#if defined(__EIGEN)
    /**
       Scalar type (required by Eigen)
    */
    using Scalar = T;
#endif
    
    /**
       Default constructor
    */
    ScalarType() noexcept = default;

    /**
       Default copy constructor
    */
    ScalarType(const ScalarType<T,rows,cols>& other) noexcept = default;
    
    /**
       Copy constructor (with implicit casting)
    */
    template<typename S>
    ScalarType(const ScalarType<S,rows,cols>& other) noexcept
    {
        for (int i=0; i<rows; i++) {
            for (int j=0; j<cols; j++) {
                data[i][j] = other.data[i][j];
            }
        }
    }
    
    /**
       Scalar constructor
    */
    ScalarType(const T& other) {
        for (int i=0; i<rows; i++) {
            for (int j=0; j<cols; j++) {
                data[i][j] = other;
            }
        }
    }

    /**
       Print to string
    */
    friend std::ostream& operator<< (std::ostream& stream, const ScalarType<T,rows,cols>& obj) {
        stream << "[";
        for (std::size_t i=0; i<rows; i++)
            for (std::size_t j=0; j<cols; j++)
                stream << obj.data[i][j] << (j<cols-1 ? "," : (i<rows-1 ? ";" : "]"));
        return stream;
    }

    /**
       Unary addition operator
    */
    inline ScalarType<T,rows,cols> operator+() const
    {
        return ScalarType<T,rows,cols>(*this);
    }

    /**
       Unary subtraction operator
    */
    inline ScalarType<T,rows,cols> operator-() const
    {
        ScalarType<T,rows,cols> tmp;
        for (std::size_t i=0; i<rows; i++)
            for (std::size_t j=0; j<cols; j++)
                tmp.data[i][j] = -this->data[i][j];
        return tmp;
    }
    
    /**
       Binary addition-assignment operator

       A[rows][cols] := A[rows][cols] + B[rows][cols]

       whereby the dimension of matrices A and B must coincide.
    */
    template <typename S>
    inline ScalarType<T,rows,cols>& operator+=(const ScalarType<S,rows,cols>& other)
    {
        for (int i=0; i<rows; i++)
            for (int j=0; j<cols; j++)
                data[i][j] += other.data[i][j];
        return *this;
    }

    /**
       Binary subraction-assignment operator
       
       A[rows][cols] := A[rows][cols] - B[rows][cols]

       whereby the dimension of matrices A and B must coincide.
    */
    template <typename S>
    inline ScalarType<T,rows,cols>& operator-=(const ScalarType<S,rows,cols>& other)
    {
        for (int i=0; i<rows; i++)
            for (int j=0; j<cols; j++)
                data[i][j] -= other.data[i][j];
        return *this;
    }

    /**
       Binary multiplication-assignment operator

       A[rows][cols] := A[rows][cols] + B[cols][cols]

       whereby the dimension of matrices A and B must be compatible.
    */
    template <typename S>
    inline ScalarType<T,rows,cols>& operator*=(const ScalarType<S,cols,cols>& other)
    {
        ScalarType<T,rows,cols> tmp(0);
        for (int i=0; i<rows; i++)
            for (int j=0; j<cols; j++)
                for (int l=0; l<cols; l++)
                    tmp.data[i][j] += data[i][l] * other.data[l][j];
        std::swap(data,tmp.data);
        return *this;
    }

    /**
       Scalar addition-assignment operator
       
       A[rows][cols] := A[rows][cols] + b

       whereby matrix A and scalar b can have different data types
    */
    inline ScalarType<T, rows, cols>&
    operator+=(const T& other)
    {
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                data[i][j] += other;
        return *this;
    }
    
    /**
       Scalar subtraction-assignment operator
       
       A[rows][cols] := A[rows][cols] - b
       
       whereby matrix A and scalar b can have different data types
    */
    inline ScalarType<T, rows, cols>&
    operator-=(const T& other)
    {
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                data[i][j] -= other;
        return *this;
    }
    
    /**
       Scalar multiplication-assignment operator
       
       A[rows][cols] := A[rows][cols] * b

       whereby matrix A and scalar b can have different data types
    */
    inline ScalarType<T, rows, cols>&
    operator*=(const T& other)
    {
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                data[i][j] *= other;
        return *this;
    }

    /**
       Scalar division-assignment operator
       
       A[rows][cols] := A[rows][cols] / b

       whereby matrix A and scalar b can have different data types
    */
    inline ScalarType<T, rows, cols>&
    operator/=(const T& other)
    {
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                data[i][j] /= other;
        return *this;
    }
};

/**
   Comparison of two custom scalar types
*/
template<typename S, typename T, int rows, int cols>
inline bool operator==(const ScalarType<S,rows,cols>& x,
                       const ScalarType<T,rows,cols>& y)
{    
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            if (x.data[i][j] != y.data[i][j]) return false;
    return true;
}

/**
   Comparison of two custom scalar types
*/
template<typename S, typename T, int rows, int cols>
inline bool operator!=(const ScalarType<S,rows,cols>& x,
                       const ScalarType<T,rows,cols>& y)
{
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            if (x.data[i][j] == y.data[i][j]) return false;
    return true;
}

/**
   Addition of two custom scalar types

   C[rows][cols] := A[rows][cols] + B[rows][cols]

   whereby matrices A, B and C can have different data types
*/
template<typename S, typename T, int rows, int cols>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator+(const ScalarType<S,rows,cols>& x,
          const ScalarType<T,rows,cols>& y)
{
    return ScalarType<typename std::common_type<S,T>::type,rows,cols>(x) += y;
}

/**
   Addition of a custom scalar type and an arithmetic type

   C[rows][cols] := A[rows][cols] + b

   whereby matrices A and C and scalar b can have different data types
*/
template<typename S, typename T, int rows, int cols>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator+(const ScalarType<S,rows,cols>& x,
          const T& y)
{
    return ScalarType<typename std::common_type<S,T>::type,rows,cols>(x) += y;
}

/**
   Addition of an arithmetic type and a custom scalar type

   C[rows][cols] := a + B[rows][cols]

   whereby matrices B and C and scalar a can have different data types
*/
template<typename S, typename T, int rows, int cols>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator+(const S& x,
          const ScalarType<T,rows,cols>& y)
{
    return ScalarType<typename std::common_type<S,T>::type,rows,cols>(y) += x;
}

/**
   Subtraction of two custom scalar types

   A[rows][cols] := B[rows][cols] - C[rows][cols]

   whereby matrices A, B and C can have different data types
*/
template<typename S, typename T, int rows, int cols>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator-(const ScalarType<S,rows,cols>& x,
          const ScalarType<T,rows,cols>& y)
{
    return ScalarType<typename std::common_type<S,T>::type,rows,cols>(x) -= y;
}

/**
   Subtraction of a custom scalar type and an arithmetic type

   C[rows][cols] := A[rows][cols] - b

   whereby matrices A and C and scalar b can have different data types
*/
template<typename S, typename T, int rows, int cols>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator-(const ScalarType<S,rows,cols>& x,
          const T& y)
{
    return ScalarType<typename std::common_type<S,T>::type,rows,cols>(x) -= y;
}

/**
   Subtraction of an arithmetic type and a custom scalar type

   C[rows][cols] := a - B[rows][cols]

   whereby matrices B and C and scalar a can have different data types
*/
template<typename S, typename T, int rows, int cols>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator-(const S& x,
          const ScalarType<T,rows,cols>& y)
{
    return ScalarType<typename std::common_type<S,T>::type,rows,cols>(y) -= x;
}

/**
   Multiplication of two custom scalar types

   C[rows][cols] := A[rows][k] * B[k][cols]

   whereby matrices A, B and C must have the same data type
*/
template<typename T, int rows, int cols, int k>
inline ScalarType<T,rows,cols>
operator*(const ScalarType<T,rows,k>& x,
          const ScalarType<T,k,cols>& y)
{
#if defined(__XSMM)
    ScalarType<T,rows,cols> tmp;
    xsmm_set<T,cols,rows,k,
             LIBXSMM_GEMM_FLAG_NONE,
             LIBXSMM_PREFETCH_AUTO
             >::xmm((const T *const)&y, (const T *const)&x, (T *const)&tmp,
                    (const T *const)&y+1, (const T *const)&x+1, (T *const)&tmp);
#else
    ScalarType<T,rows,cols> tmp(0);
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            for (int l=0; l<k; l++)
                tmp.data[i][j] += x.data[i][l] * y.data[l][j];
#endif
    return tmp;        
}

/**
   Multiplication of two custom scalar types

   C[rows][cols] := A[rows][k] * B[k][cols]

   whereby matrices A, B and C can have different data types
*/
template<typename S, typename T, int rows, int cols, int k>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator*(const ScalarType<S,rows,k>& x,
          const ScalarType<T,k,cols>& y)
{
    ScalarType<typename std::common_type<S,T>::type,rows,cols> tmp(0);
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            for (int l=0; l<k; l++)
                tmp.data[i][j] += x.data[i][l] * y.data[l][j];
    return tmp;
}

/**
   Multiplication of a custom scalar type and an arithmetic type

   C[rows][cols] := A[rows][cols] * b

   whereby matrices A and C can have different data types and scalar b is float
*/
template<typename T, int rows, int cols>
inline ScalarType<typename std::common_type<float,T>::type,rows,cols>
operator*(const ScalarType<T,rows,cols>& x,
          const float& y)
{
    return ScalarType<typename std::common_type<float,T>::type,rows,cols>(x) *= y;
}

/**
   Multiplication of a custom scalar type and an arithmetic type

   C[rows][cols] := A[rows][cols] * b

   whereby matrices A and C can have different data types and scalar b is double
*/
template<typename T, int rows, int cols>
inline ScalarType<typename std::common_type<double,T>::type,rows,cols>
operator*(const ScalarType<T,rows,cols>& x,
          const double& y)
{
    return ScalarType<typename std::common_type<double,T>::type,rows,cols>(x) *= y;
}

/**
   Multiplication of an arithmetic type and a custom scalar type

   C[rows][cols] := a - B[rows][cols]

   whereby matrices B and C can have different data types and scalar a s float
*/
template<typename T, int rows, int cols>
inline ScalarType<typename std::common_type<float,T>::type,rows,cols>
operator*(const float& x,
          const ScalarType<T,rows,cols>& y)
{
    return ScalarType<typename std::common_type<float,T>::type,rows,cols>(y) *= x;
}

/**
   Multiplication of an arithmetic type and a custom scalar type

   C[rows][cols] := a - B[rows][cols]

   whereby matrices B and C can have different data types and scalar a is double
*/
template<typename T, int rows, int cols>
inline ScalarType<typename std::common_type<double,T>::type,rows,cols>
operator*(const double& x,
          const ScalarType<T,rows,cols>& y)
{
    return ScalarType<typename std::common_type<double,T>::type,rows,cols>(y) *= x;
}

/**
   Division of a custom scalar type by an arithmetic type

   C[rows][cols] := A[rows][cols] * b

   whereby matrices A and C and scalar b can have different data types
*/
template<typename S, typename T, int rows, int cols>
inline ScalarType<typename std::common_type<S,T>::type,rows,cols>
operator/(const ScalarType<S,rows,cols>& x,
          const T& y)
{
    return ScalarType<typename std::common_type<S,T>::type,rows,cols>(x) /= y;
}

/**
   Complex conjugate of a custom scalar type
*/
template<int rows, int cols>
inline ScalarType<float, rows, cols>
conj(const ScalarType<float, rows, cols>& a) { return a; }

template<int rows, int cols>
inline ScalarType<double, rows, cols>
conj(const ScalarType<double, rows, cols>& a) { return a; }

template<int rows, int cols>
inline ScalarType<long double, rows, cols>
conj(const ScalarType<long double, rows, cols>& a) { return a; }

template<int rows, int cols>
inline ScalarType<std::complex<float>, rows, cols>
conj(const ScalarType<std::complex<float>, rows, cols>& a) { return std::conj(a); }

template<int rows, int cols>
inline ScalarType<std::complex<double>, rows, cols>
conj(const ScalarType<std::complex<double>, rows, cols>& a) { return std::conj(a); }

template<int rows, int cols>
inline ScalarType<std::complex<long double>, rows, cols>
conj(const ScalarType<std::complex<long double>, rows, cols>& a) { return std::conj(a); }

/**
   Real part of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
real(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::real(a.data[i][j]);
    return tmp;
}

/**
   Imaginary part of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
imag(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::imag(a.data[i][j]);
    return tmp;
}

/**
   Square of absolute value of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
abs2(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = a.data[i][j] * a.data[i][j];
    return tmp;
}

/**
   Absolute value of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
abs(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::abs(a.data[i][j]);
    return tmp;
}

/**
   Square root of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
sqrt(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::sqrt(a.data[i][j]);
    return tmp;
}

/**
   Reciprocal square root of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
rsqrt(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = T(1)/std::sqrt(a.data[i][j]);
    return tmp;
}

/**
   Natural logarithm of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
log(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::log(a.data[i][j]);
    return tmp;
}

/**
   Logarithm to base 10 of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
log10(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::log10(a.data[i][j]);
    return tmp;
}

/**
   Exponent of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
exp(const fdbb::ScalarType<T,rows,cols>& a,
    const fdbb::ScalarType<T,rows,cols>& b) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::exp(a.data[i][j], b.data[i][j]);
    return tmp;
}

/**
   Exponent of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
exp(const fdbb::ScalarType<T,rows,cols>& a, const T& b) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::exp(a.data[i][j], b);
    return tmp;
}

/**
   Square of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
square(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = a.data[i][j] * a.data[i][j];
    return tmp;
}

/**
   Cube of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
cube(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = a.data[i][j] * a.data[i][j] * a.data[i][j];
    return tmp;
}

/**
   Inverse of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
inverse(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = T(1)/a.data[i][j];
    return tmp;
}

/**
   Sine of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
sin(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::sin(a.data[i][j]);
    return tmp;
}

/**
   Cosine of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
cos(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::cos(a.data[i][j]);
    return tmp;
}

/**
   Tangent of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
tan(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::tan(a.data[i][j]);
    return tmp;
}

/**
   Inverse sine of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
asin(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::asin(a.data[i][j]);
    return tmp;
}

/**
   Inverse cosine of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
acos(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::acos(a.data[i][j]);
    return tmp;
}

/**
   Inverse tangent of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
atan(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::atan(a.data[i][j]);
    return tmp;
}

/**
   Hyperbolic sine of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
sinh(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::sinh(a.data[i][j]);
    return tmp;
}

/**
   Hyperbolic cosine of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
cosh(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::cosh(a.data[i][j]);
    return tmp;
}

/**
   Hyperbolic tangent of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
tanh(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::tanh(a.data[i][j]);
    return tmp;
}

/**
   Floor of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
floor(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::floor(a.data[i][j]);
    return tmp;
}

/**
   Ceiling of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
ceil(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::ceil(a.data[i][j]);
    return tmp;
}

/**
   Rounding of a custom scalar type
*/
template<typename T, int rows, int cols>
inline fdbb::ScalarType<T,rows,cols>
round(const fdbb::ScalarType<T,rows,cols>& a) {
    fdbb::ScalarType<T,rows,cols> tmp;
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            tmp.data[i][j] = std::round(a.data[i][j]);
    return tmp;
}

} // namespace fdbb

#if defined(__EIGEN)

//------------------------------------------------------------------------------
// EIGEN: Custom Scalar Type Traits
//------------------------------------------------------------------------------

namespace Eigen {

/**
   Specialization of NumTraits for custom scalar type

   The NumTraits structure holds information about the various numeric
   (i.e. scalar) types allowed by Eigen. This specialization
   overwrites the default values for the custom scalar type
   fdbb::ScalarType<T,rows,cols>.

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct NumTraits<fdbb::ScalarType<T,rows,cols> >
    : NumTraits<T> // permits to get the epsilon, dummy_precision, lowest, highest functions
{
    typedef fdbb::ScalarType<T,rows,cols> Real;
    typedef fdbb::ScalarType<T,rows,cols> NonInteger;
    typedef fdbb::ScalarType<T,rows,cols> Nested;
    enum {
        IsComplex = 0,
        IsInteger = 0,
        IsSigned  = 1,
        RequireInitialization = 1,
        ReadCost =   rows*cols,
        AddCost  = 3*rows*cols,
        MulCost  = 3*rows*cols
    };
};

/**
   Specialization of the return type deducation traits of the
   add-assignment operator between a custom scalar type
   fdbb::ScalarType<T,rows,cols> and an arithmetic type T

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<fdbb::ScalarType<T,rows,cols>,
                            T,
                            internal::add_assign_op<fdbb::ScalarType<T,rows,cols>,
                                                    T > >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   sub-assignment operator between an arithmetic type T and a custom
   scalar type fdbb::ScalarType<T,rows,cols>

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<fdbb::ScalarType<T,rows,cols>,
                            T,
                            internal::sub_assign_op<fdbb::ScalarType<T,rows,cols>,
                                                    T > >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   add-operator between a custom scalar type
   fdbb::ScalarType<T,rows,cols> and an arithmetic type T

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<T,
                            fdbb::ScalarType<T,rows,cols>,
                            internal::scalar_sum_op<T,
                                                    fdbb::ScalarType<T,rows,cols> > >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   add-operator between an arithmetic type T and a custom scalar type
   fdbb::ScalarType<T,rows,cols>

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<fdbb::ScalarType<T,rows,cols>,
                            T,
                            internal::scalar_sum_op<fdbb::ScalarType<T,rows,cols>,
                                                    T> >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   sub-operator between a custom scalar type
   fdbb::ScalarType<T,rows,cols> and an arithmetic type T

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<T,
                            fdbb::ScalarType<T,rows,cols>,
                            internal::scalar_difference_op<T,
                                                           fdbb::ScalarType<T,rows,cols> > >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   sub-operator between an arithmetic type T and a custom scalar type
   fdbb::ScalarType<T,rows,cols>

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<fdbb::ScalarType<T,rows,cols>,
                            T,
                            internal::scalar_difference_op<fdbb::ScalarType<T,rows,cols>,
                                                           T> >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   mul-operator between two custom scalar types
   fdbb::ScalarType<T,rows,k> and fdbb::ScalarType<T,k,cols>, whereby
   the result of of custom scalar type fdbb::ScalarType<T,rows,cols>

   This specialization covers the non-default case (m != k != n),
   which is handled automatically by the Eigen library.

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols, int k>
struct ScalarBinaryOpTraits<typename std::enable_if<rows!=k || k!=cols,fdbb::ScalarType<T,rows,k> >::type,
                            typename std::enable_if<rows!=k || k!=cols,fdbb::ScalarType<T,k,cols> >::type,
                            internal::scalar_product_op<fdbb::ScalarType<T,rows,k>,
                                                        fdbb::ScalarType<T,k,cols> > >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   mul-operator between a custom scalar type
   fdbb::ScalarType<T,rows,cols> and an arithmetic type T

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<T,
                            fdbb::ScalarType<T,rows,cols>,
                            internal::scalar_product_op<T,
                                                        fdbb::ScalarType<T,rows,cols> > >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   mul-operator between an arithmetic type T and a custom scalar type
   fdbb::ScalarType<T,rows,cols>

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html
*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<fdbb::ScalarType<T,rows,cols>,
                            T,
                            internal::scalar_product_op<fdbb::ScalarType<T,rows,cols>,
                                                        T> >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

/**
   Specialization of the return type deducation traits of the
   div-operator between a custom scalar type
   fdbb::ScalarType<T,rows,cols> and an arithmetic type T

   @note
   The details of how to enable custom scalar types is given at
   https://eigen.tuxfamily.org/dox/TopicCustomizing_CustomScalar.html

*/
template<typename T, int rows, int cols>
struct ScalarBinaryOpTraits<fdbb::ScalarType<T,rows,cols>,
                            T,
                            internal::scalar_quotient_op<fdbb::ScalarType<T,rows,cols>,
                                                         T> >
{
    typedef fdbb::ScalarType<T,rows,cols> ReturnType;
};

//------------------------------------------------------------------------------
// EIGEN INTERNALS
//------------------------------------------------------------------------------

namespace internal {

/**
   Specialization of the abs2 implementation for custom scalar types
   fdbb::ScalarType<T,rows,cols> with real-valued type T
*/ 
template<typename T, int rows, int cols>
struct abs2_impl_default<fdbb::ScalarType<T,rows,cols>,false>
{
    typedef fdbb::ScalarType<T,rows,cols> Scalar;
    typedef typename NumTraits<Scalar>::Real RealScalar;
    EIGEN_DEVICE_FUNC
    static inline RealScalar run(const Scalar& x)
    {
        return fdbb::abs2(x);
    }
};

/**
   Specialization of the abs2 implementation for custom scalar types
   fdbb::ScalarType<T,rows,cols> with complex-valued type T
*/ 
template<typename T, int rows, int cols>
struct abs2_impl_default<fdbb::ScalarType<T,rows,cols>,true>
{
    typedef fdbb::ScalarType<T,rows,cols> Scalar;
    typedef typename NumTraits<Scalar>::Real RealScalar;
    EIGEN_DEVICE_FUNC
    static inline RealScalar run(const Scalar& x)
    {
        return fdbb::abs2(fdbb::real(x)) + fdbb::abs2(fdbb::imag(x));
    }
};

/**
   Specialized GEBP traits for custom scalar types
   fdbb::ScalarType<T,rows,k> and fdbb::ScalarType<T,k,cols>
   
   @note
   The GEBP traits are declared in file
   Eigen/src/Core/products/GeneralBlockPanelKernel.h
*/

template<typename T, int rows, int cols, int k1, int k2, bool ConjugateLhs, bool ConjugateRhs>
class gebp_traits<fdbb::ScalarType<T,rows,k1>,
                  fdbb::ScalarType<T,k2,cols>,
                  ConjugateLhs, ConjugateRhs>
{
public:
    typedef fdbb::ScalarType<T,rows,k1> LhsScalar;
    typedef fdbb::ScalarType<T,k2,cols> RhsScalar;
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;
    
    enum {
        ConjLhs = ConjugateLhs,
        ConjRhs = ConjugateRhs,
        Vectorizable = false,
        LhsPacketSize = 1,
        RhsPacketSize = 1,
        ResPacketSize = 1,
        NumberOfRegisters = 1,
        nr = 1,
        mr = 1,
        LhsProgress = 1,
        RhsProgress = 1
    };

    typedef LhsScalar LhsPacket;
    typedef RhsScalar RhsPacket;
    typedef ResScalar ResPacket;
};

/**
   Specialized GEBP traits for intrinsic type T and custom scalar
   types fdbb::ScalarType<T,rows,cols>
   
   @note
   The GEBP traits are declared in file
   Eigen/src/Core/products/GeneralBlockPanelKernel.h
*/

template<typename T, int rows, int cols, bool ConjugateLhs, bool ConjugateRhs>
class gebp_traits<T,
                  fdbb::ScalarType<T,rows,cols>,
                  ConjugateLhs, ConjugateRhs>
{
public:
    typedef T LhsScalar;
    typedef fdbb::ScalarType<T,rows,cols> RhsScalar;
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;
    
    enum {
        ConjLhs = ConjugateLhs,
        ConjRhs = ConjugateRhs,
        Vectorizable = false,
        LhsPacketSize = 1,
        RhsPacketSize = 1,
        ResPacketSize = 1,
        NumberOfRegisters = 1,
        nr = 1,
        mr = 1,
        LhsProgress = 1,
        RhsProgress = 1
    };

    typedef LhsScalar LhsPacket;
    typedef RhsScalar RhsPacket;
    typedef ResScalar ResPacket;
};

/**
   Specialized GEBP traits for custom scalar type
   fdbb::ScalarType<T,rows,cols> and intrinsic type T
   
   @note
   The GEBP traits are declared in file
   Eigen/src/Core/products/GeneralBlockPanelKernel.h
*/

template<typename T, int rows, int cols, bool ConjugateLhs, bool ConjugateRhs>
class gebp_traits<fdbb::ScalarType<T,rows,cols>,
                  T,
                  ConjugateLhs, ConjugateRhs>
{
public:
    typedef fdbb::ScalarType<T,rows,cols> LhsScalar;
    typedef T RhsScalar;
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;
    
    enum {
        ConjLhs = ConjugateLhs,
        ConjRhs = ConjugateRhs,
        Vectorizable = false,
        LhsPacketSize = 1,
        RhsPacketSize = 1,
        ResPacketSize = 1,
        NumberOfRegisters = 1,
        nr = 1,
        mr = 1,
        LhsProgress = 1,
        RhsProgress = 1
    };

    typedef LhsScalar LhsPacket;
    typedef RhsScalar RhsPacket;
    typedef ResScalar ResPacket;
};

/**
   Specialized GEBP kernel for custom scalar types
   fdbb::ScalarType<T,rows,k1> and fdbb::ScalarType<T,k2,cols>
   
   @note
   The GEBP kernel is declared in file
   Eigen/src/Core/products/GeneralBlockPanelKernel.h
*/
template<typename T, int _rows, int _cols, int _k1, int _k2,
         typename Index, typename DataMapper, int mr, int nr, bool ConjugateLhs, bool ConjugateRhs>
struct gebp_kernel<fdbb::ScalarType<T,_rows,_k1>,
                   fdbb::ScalarType<T,_k2,_cols>,
                   Index, DataMapper, mr, nr, ConjugateLhs, ConjugateRhs>
{
    typedef fdbb::ScalarType<T,_rows,_k1> LhsScalar;
    typedef fdbb::ScalarType<T,_k2,_cols> RhsScalar;
    
    typedef gebp_traits<LhsScalar,RhsScalar,ConjugateLhs,ConjugateRhs> Traits;
    typedef typename Traits::ResScalar ResScalar;
    
    EIGEN_DONT_INLINE
    void operator()(const DataMapper& res, const LhsScalar* blockA, const RhsScalar* blockB,
                    Index rows, Index depth, Index cols, T alpha,
                    Index strideA=-1, Index strideB=-1, Index offsetA=0, Index offsetB=0)
    {
        if(rows==0 || cols==0 || depth==0)
            return;
        
        ResScalar tmp;
        
        if(strideA==-1) strideA = depth;
        if(strideB==-1) strideB = depth;

        for(Index i=0; i<rows; ++i)
        {
            for(Index j=0; j<cols; ++j)
            {                
                const LhsScalar *A = blockA + i*strideA + offsetA;
                const RhsScalar *B = blockB + j*strideB + offsetB;
                
#if defined(__XSMM)                
                fdbb::xsmm_set<T,_cols,_rows,_k1,
                               LIBXSMM_GEMM_FLAG_NONE,
                               LIBXSMM_PREFETCH_AUTO>::xmm(&B[0].data[0][0], &A[0].data[0][0], &tmp.data[0][0],
                                                           &B[1].data[0][0], &A[1].data[0][0], &tmp.data[0][0]);
                for(Index k=1; k<depth; k++)
                {
                    fdbb::xsmm_add<T,_cols,_rows,_k1,
                                   LIBXSMM_GEMM_FLAG_NONE,
                                   LIBXSMM_PREFETCH_AUTO>::xmm(&B[k].data[0][0], &A[k].data[0][0], &tmp.data[0][0],
                                                               &B[k+1].data[0][0], &A[k+1].data[0][0], &tmp.data[0][0]);
                }
#else
                tmp = A[0]*B[0];
                for(Index k=1; k<depth; k++)
                {
                    tmp += A[k]*B[k];
                }
#endif                
                res(i,j) += alpha * tmp;
            }
        }
    }
};

/**
   Specialized GEBP kernel for intrinsic type T and custom scalar type
   fdbb::ScalarType<T,rows,cols>
   
   @note
   The GEBP kernel is declared in file
   Eigen/src/Core/products/GeneralBlockPanelKernel.h
*/
template<typename T, int _rows, int _cols,
         typename Index, typename DataMapper, int mr, int nr, bool ConjugateLhs, bool ConjugateRhs>
struct gebp_kernel<T,
                   fdbb::ScalarType<T,_rows,_cols>,
                   Index, DataMapper, mr, nr, ConjugateLhs, ConjugateRhs>
{
    typedef T LhsScalar;
    typedef fdbb::ScalarType<T,_rows,_cols> RhsScalar;
    
    typedef gebp_traits<LhsScalar,RhsScalar,ConjugateLhs,ConjugateRhs> Traits;
    typedef typename Traits::ResScalar ResScalar;
    
    EIGEN_DONT_INLINE
    void operator()(const DataMapper& res, const LhsScalar* blockA, const RhsScalar* blockB,
                    Index rows, Index depth, Index cols, T alpha,
                    Index strideA=-1, Index strideB=-1, Index offsetA=0, Index offsetB=0)
    {
        if(rows==0 || cols==0 || depth==0)
            return;
        
        if(strideA==-1) strideA = depth;
        if(strideB==-1) strideB = depth;

        for(Index i=0; i<rows; ++i)
        {
            for(Index j=0; j<cols; ++j)
            {                
                const LhsScalar *A = blockA + i*strideA + offsetA;
                const RhsScalar *B = blockB + j*strideB + offsetB;
                
                res(i,j) += alpha * *A * *B;
            }
        }
    }
};

/**
   Specialized GEBP kernel for custom scalar type
   fdbb::ScalarType<T,rows,cols> and intrinsic type T
   
   @note
   The GEBP kernel is declared in file
   Eigen/src/Core/products/GeneralBlockPanelKernel.h
*/
template<typename T, int _rows, int _cols,
         typename Index, typename DataMapper, int mr, int nr, bool ConjugateLhs, bool ConjugateRhs>
struct gebp_kernel<fdbb::ScalarType<T,_rows,_cols>,
                   T,
                   Index, DataMapper, mr, nr, ConjugateLhs, ConjugateRhs>
{
    typedef fdbb::ScalarType<T,_rows,_cols> LhsScalar;
    typedef T RhsScalar;
    
    typedef gebp_traits<LhsScalar,RhsScalar,ConjugateLhs,ConjugateRhs> Traits;
    typedef typename Traits::ResScalar ResScalar;
    
    EIGEN_DONT_INLINE
    void operator()(const DataMapper& res, const LhsScalar* blockA, const RhsScalar* blockB,
                    Index rows, Index depth, Index cols, T alpha,
                    Index strideA=-1, Index strideB=-1, Index offsetA=0, Index offsetB=0)
    {
        if(rows==0 || cols==0 || depth==0)
            return;
        
        if(strideA==-1) strideA = depth;
        if(strideB==-1) strideB = depth;

        for(Index i=0; i<rows; ++i)
        {
            for(Index j=0; j<cols; ++j)
            {                
                const LhsScalar *A = blockA + i*strideA + offsetA;
                const RhsScalar *B = blockB + j*strideB + offsetB;
                
                res(i,j) += alpha * *A * *B;
            }
        }
    }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrices with intrinsic scalar type and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,rows,cols>
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Derived>
struct generic_product_impl_base<Matrix<T,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef Matrix<T,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and a dense Eigen matrices with
   intrinsic scalar type
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 Matrix<T,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef Matrix<T,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with intrinsic scalar type and a sparse Eigen matrix with custom
   scalar type fdbb::ScalarType<T,rows,cols>
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename Derived>
struct generic_product_impl_base<Matrix<T,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 SparseMatrix<fdbb::ScalarType<T,_rows,_cols>,_Options2,_StorageIndex2>,
                                 Derived>
{
    typedef Matrix<T,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T,_rows,_cols>,_Options2,_StorageIndex2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and a dense Eigen matrix with
   intrinsic scalar type
*/
template<typename T, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Derived>
struct generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T,_rows,_cols>,_Options1,_StorageIndex1>,
                                 Matrix<T,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,                                 
                                 Derived>
{
    typedef SparseMatrix<fdbb::ScalarType<T,_rows,_cols>,_Options1,_StorageIndex1> Lhs;
    typedef Matrix<T,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with intrinsic scalar type and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,                                 
                                 Derived>
{
    typedef Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and a dense Eigen
   matrix with intrinsic scalar type
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with intrinsic scalar type and an Eigen
   CwiseUnaryType with a sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                              >,
                                 Derived>
{
    typedef Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const  SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryType with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and a dense Eigen
   matrix with intrinsic scalar type
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                              >,
                                 Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const  SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1> > Lhs;
    typedef Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of two
   dense Eigen matrices with custom scalar types
   fdbb::ScalarType<T,rows,k1> and fdbb::ScalarType<T,k2,cols>
*/
template<typename T, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 Matrix<fdbb::ScalarType<T,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and a sparse Eigen matrix with
   intrinsic scalar type
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 SparseMatrix<T,_Options2,_StorageIndex2>,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef SparseMatrix<T,_Options2,_StorageIndex2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   sparse Eigen matrix with intrinsic scalar type and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,rows,cols>
*/
template<typename T, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Derived>
struct generic_product_impl_base<SparseMatrix<T,_Options1,_StorageIndex1>,
                                 Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef SparseMatrix<T,_Options1,_StorageIndex1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and a sparse Eigen matrix with custom
   scalar type fdbb::ScalarType<T,k2,cols>
*/
template<typename T, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 SparseMatrix<fdbb::ScalarType<T,_k2,_cols>,_Options2,_StorageIndex2>,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T,_k2,_cols>,_Options2,_StorageIndex2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   sparse Eigen matrix with custom scalar types
   fdbb::ScalarType<T,rows,k1> and a dense Eigen matrix with custom
   scalar type fdbb::ScalarType<T,k2,cols>
*/
template<typename T, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Derived>
struct generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T,_rows,_k1>,_Options1,_StorageIndex1>,
                                 Matrix<fdbb::ScalarType<T,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef SparseMatrix<fdbb::ScalarType<T,_rows,_k1>,_Options1,_StorageIndex1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and an Eigen CwiseUnaryOp with a
   dense Eigen matrix with intrinsic scalar type as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar
   type as XprType and a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols>
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and an Eigen CwiseUnaryOp with a dense
   Eigen matrix with custom scalar type fdbb::ScalarType<T,k2,cols> as
   XprType
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,k2,cols>
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,                                 
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and an Eigen CwiseUnaryOp with a
   sparse Eigen matrix with intrinsic scalar type as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const SparseMatrix<T2, _Options2, _StorageIndex2>
                                              >,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<T2, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryType with a sparse Eigen matrix with intrinsic
   scalar type as XprType and a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols>
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const SparseMatrix<T1, _Options1, _StorageIndex1>
                                              >,
                                 Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const  SparseMatrix<T1, _Options1, _StorageIndex1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and an Eigen CwiseUnaryOp with a sparse
   Eigen matrix with custom scalar type fdbb::ScalarType<T,k2,cols> as
   XprType
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                              >,
                                 Derived>
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryType with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,k2,cols>
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                              >,
                                 Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const  SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   sparse Eigen matrix with intrinsic scalar type and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<SparseMatrix<T1, _Options1, _StorageIndex1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef SparseMatrix<T1, _Options1, _StorageIndex1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryType with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and a sparse Eigen
   matrix with intrinsic scalar type
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 SparseMatrix<T2, _Options2, _StorageIndex2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef SparseMatrix<T2, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and an Eigen CwiseUnaryOp with a
   dense Eigen matrix with intrinsic scalar type as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryType with a dense Eigen matrix with intrinsic
   scalar type as XprType and a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols>
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of a
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and an Eigen CwiseUnaryOp with a dense
   Eigen matrix with custom scalar type fdbb::ScalarType<T,k2,cols> as
   XprType
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>,
                                 CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryType with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and a sparse Eigen
   matrix with custom scalar type fdbb::ScalarType<T,k2,cols>
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar
   type as XprType and an Eigen CwiseUnaryOp with a dense Eigen matrix
   with custom scalar type fdbb::ScalarType<T,rows,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,                                 
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType an Eigen CwiseUnaryOp
   with a dense Eigen matrix with intrinsic scalar type as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar
   type as XprType and an Eigen CwiseUnaryOp with a sparse Eigen
   matrix with custom scalar type fdbb::ScalarType<T,rows,cols> as
   XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<T1,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryType with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar type
   as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const  SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<T2,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,k2,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T2(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T2(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and an Eigen
   CwiseUnaryOp with a sparse Eigen matrix with intrinsic scalar type
   as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const SparseMatrix<T2, _Options2, _StorageIndex2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_cols>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const SparseMatrix<T2, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with intrinsic scalar
   type as XprType and an Eigen CwiseUnaryOp with a dense Eigen matrix
   with custom scalar type fdbb::ScalarType<T,rows,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const SparseMatrix<T1, _Options1, _StorageIndex1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const SparseMatrix<T1, _Options1, _StorageIndex1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_rows,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and an Eigen
   CwiseUnaryOp with a sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,k2,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_k1>,_Rows1,_Cols1,_Options1,_MaxRows1,_MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   This base class provides default implementations for evalTo, addTo,
   subTo, in terms of scaleAndAddTo for the specialized case of an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,k2,cols> as XprType
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, typename Derived>
struct generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                              const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                              >,
                                 CwiseUnaryOp<UnaryOp2,
                                              const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2>
                                              >,
                                 Derived>
{
    typedef CwiseUnaryOp<UnaryOp1, const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_k2,_cols>,_Rows2,_Cols2,_Options2,_MaxRows2,_MaxCols2> > Rhs;
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { dst.setZero(); scaleAndAddTo(dst, lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst,lhs, rhs, T1(1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    { scaleAndAddTo(dst, lhs, rhs, T1(-1)); }
    
    template<typename Dst>
    static EIGEN_STRONG_INLINE void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    { Derived::scaleAndAddTo(dst,lhs,rhs,alpha); }
};

/**
   Specialization of generic_product_impl for "large" GEMM, i.e.,
   implementation of the high level wrapper to general_matrix_matrix_product
*/
template<typename T, typename Index, typename Gemm,
         int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Dest, typename BlockingType>
struct gemm_functor<T, Index, Gemm,
                    Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                    Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                    Dest, BlockingType>
{
    typedef Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    typedef typename Dest::Scalar Scalar;
    
    gemm_functor(const Lhs& lhs, const Rhs& rhs, Dest& dest, const T& actualAlpha, BlockingType& blocking)
    : m_lhs(lhs), m_rhs(rhs), m_dest(dest), m_actualAlpha(actualAlpha), m_blocking(blocking)
    {}
    
    void initParallelSession(Index num_threads) const
    {
        m_blocking.initParallel(m_lhs.rows(), m_rhs.cols(), m_lhs.cols(), num_threads);
        m_blocking.allocateA();
    }
    
    void operator() (Index row, Index rows, Index col=0, Index cols=-1, GemmParallelInfo<Index>* info=0) const
    {
        if(cols==-1)
            cols = m_rhs.cols();

        Gemm::run(rows, cols, m_lhs.cols(),
                  &m_lhs.coeffRef(row,0), m_lhs.outerStride(),
                  &m_rhs.coeffRef(0,col), m_rhs.outerStride(),
                  (Scalar*)&(m_dest.coeffRef(row,col)), m_dest.outerStride(),
                  m_actualAlpha, m_blocking, info);
    }
    
    typedef typename Gemm::Traits Traits;
    
protected:
    const Lhs& m_lhs;
    const Rhs& m_rhs;
    Dest& m_dest;
    T m_actualAlpha;
    BlockingType& m_blocking;
};

/**
   Specialization of generic_product_impl for "large" GEMM, i.e.,
   implementation of the high level wrapper to general_matrix_matrix_product
*/
template<typename T, typename Index, typename Gemm,
         int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Dest, typename BlockingType>
struct gemm_functor<T, Index, Gemm,
                    Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                    Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                    Dest, BlockingType>
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    typedef typename Dest::Scalar Scalar;
    
    gemm_functor(const Lhs& lhs, const Rhs& rhs, Dest& dest, const T& actualAlpha, BlockingType& blocking)
    : m_lhs(lhs), m_rhs(rhs), m_dest(dest), m_actualAlpha(actualAlpha), m_blocking(blocking)
    {}
    
    void initParallelSession(Index num_threads) const
    {
        m_blocking.initParallel(m_lhs.rows(), m_rhs.cols(), m_lhs.cols(), num_threads);
        m_blocking.allocateA();
    }
    
    void operator() (Index row, Index rows, Index col=0, Index cols=-1, GemmParallelInfo<Index>* info=0) const
    {
        if(cols==-1)
            cols = m_rhs.cols();

        Gemm::run(rows, cols, m_lhs.cols(),
                  &m_lhs.coeffRef(row,0), m_lhs.outerStride(),
                  &m_rhs.coeffRef(0,col), m_rhs.outerStride(),
                  (Scalar*)&(m_dest.coeffRef(row,col)), m_dest.outerStride(),
                  m_actualAlpha, m_blocking, info);
    }
    
    typedef typename Gemm::Traits Traits;
    
protected:
    const Lhs& m_lhs;
    const Rhs& m_rhs;
    Dest& m_dest;
    T m_actualAlpha;
    BlockingType& m_blocking;
};

/**
   Specialization of generic_product_impl for "large" GEMM, i.e.,
   implementation of the high level wrapper to general_matrix_matrix_product
*/
template<typename T, typename Index, typename Gemm,
         int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename Dest, typename BlockingType>
struct gemm_functor<T, Index, Gemm,
                    Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                    Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                    Dest, BlockingType>
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    typedef typename Dest::Scalar Scalar;
    
    gemm_functor(const Lhs& lhs, const Rhs& rhs, Dest& dest, const T& actualAlpha, BlockingType& blocking)
    : m_lhs(lhs), m_rhs(rhs), m_dest(dest), m_actualAlpha(actualAlpha), m_blocking(blocking)
    {}
    
    void initParallelSession(Index num_threads) const
    {
        m_blocking.initParallel(m_lhs.rows(), m_rhs.cols(), m_lhs.cols(), num_threads);
        m_blocking.allocateA();
    }
    
    void operator() (Index row, Index rows, Index col=0, Index cols=-1, GemmParallelInfo<Index>* info=0) const
    {
        if(cols==-1)
            cols = m_rhs.cols();

        Gemm::run(rows, cols, m_lhs.cols(),
                  &m_lhs.coeffRef(row,0), m_lhs.outerStride(),
                  &m_rhs.coeffRef(0,col), m_rhs.outerStride(),
                  (Scalar*)&(m_dest.coeffRef(row,col)), m_dest.outerStride(),
                  m_actualAlpha, m_blocking, info);
    }
    
    typedef typename Gemm::Traits Traits;
    
protected:
    const Lhs& m_lhs;
    const Rhs& m_rhs;
    Dest& m_dest;
    T m_actualAlpha;
    BlockingType& m_blocking;
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with intrinsic scalar type and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,rows,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2>
struct generic_product_impl<Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);

        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);

        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with custom scalar types
   fdbb::ScalarType<T,rows,cols> and a dense Eigen matrix with
   intrinsic scalar type
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2>
struct generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     Matrix<T, _Rows1, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);

        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
            
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for two
   dense Eigen matrices with custom scalar types
   fdbb::ScalarType<T,rows,k1> and fdbb::ScalarType<T,k2,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2>
struct generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows1, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);

        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);

        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with intrinsic scalar type and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp>
struct generic_product_impl<Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,                            
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,                                
                                generic_product_impl<Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,                                                    
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T1(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T1(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T1(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T1& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T1, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and an Eigen CwiseUnaryOp with a
   dense Eigen matrix with intrinsic scalar type as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp>
struct generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            CwiseUnaryOp<UnaryOp,
                                         const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,                            
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                CwiseUnaryOp<UnaryOp,
                                             const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,                                
                                generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,                                                    
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T1(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T1(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T1(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T1& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T1, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and an Eigen CwiseUnaryOp with a dense
   Eigen matrix with custom scalar type fdbb::ScalarType<T,k2,cols> as
   XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp>
struct generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,                            
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,                                
                                generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,                                                    
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T1(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T1(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T1(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T1& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T1, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar
   type as XprType and a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T2(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T2(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T2(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T2& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T2, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and a dense Eigen
   matrix with intrinsic scalar type
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T2(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T2(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T2(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T2& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T2, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,k2,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T2(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T2(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T2(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T2& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T2, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar
   type as XprType and an Eigen CwiseUnaryOp with a dense Eigen matrix
   with custom scalar type fdbb::ScalarType<T,rows,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T2(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T2(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T2(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T2& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T2, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar type
   as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T2(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T2(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T2(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T2& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T2, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,k2,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            DenseShape, DenseShape, GemmProduct>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     DenseShape, DenseShape, GemmProduct> >
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
    
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename Lhs::Scalar LhsScalar;
    typedef typename Rhs::Scalar RhsScalar;
    
    typedef internal::blas_traits<Lhs> LhsBlasTraits;
    typedef typename LhsBlasTraits::DirectLinearAccessType ActualLhsType;
    typedef typename internal::remove_all<ActualLhsType>::type ActualLhsTypeCleaned;

    typedef internal::blas_traits<Rhs> RhsBlasTraits;
    typedef typename RhsBlasTraits::DirectLinearAccessType ActualRhsType;
    typedef typename internal::remove_all<ActualRhsType>::type ActualRhsTypeCleaned;

    enum {
        MaxDepthAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(Lhs::MaxColsAtCompileTime,Rhs::MaxRowsAtCompileTime)
    };

    typedef generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,CoeffBasedProductMode> lazyproduct;

    template<typename Dst>
    static void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::evalTo(dst, lhs, rhs);
        else
        {
            dst.setZero();
            scaleAndAddTo(dst, lhs, rhs, T2(1));
        }
    }

    template<typename Dst>
    static void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::addTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst,lhs, rhs, T2(1));
    }

    template<typename Dst>
    static void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
    {
        if((rhs.rows()+dst.rows()+dst.cols())<20 && rhs.rows()>0)
            lazyproduct::subTo(dst, lhs, rhs);
        else
            scaleAndAddTo(dst, lhs, rhs, T2(-1));
    }

    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& a_lhs, const Rhs& a_rhs, const T2& alpha)
    {
        eigen_assert(dst.rows()==a_lhs.rows() && dst.cols()==a_rhs.cols());
        if(a_lhs.cols()==0 || a_lhs.rows()==0 || a_rhs.cols()==0)
            return;

        typename internal::add_const_on_value_type<ActualLhsType>::type lhs = LhsBlasTraits::extract(a_lhs);
        typename internal::add_const_on_value_type<ActualRhsType>::type rhs = RhsBlasTraits::extract(a_rhs);
        
        // Note that the original implementation computed an actual alpha as follows
        // T actualAlpha = alpha * LhsBlasTraits::extractScalarFactor(a_lhs)
        //                       * RhsBlasTraits::extractScalarFactor(a_rhs);
        
        typedef internal::gemm_blocking_space<(Dst::Flags&RowMajorBit) ? RowMajor : ColMajor,LhsScalar,RhsScalar,
                                              Dst::MaxRowsAtCompileTime,Dst::MaxColsAtCompileTime,MaxDepthAtCompileTime> BlockingType;
        
        typedef internal::gemm_functor<
            T2, Index,
            internal::general_matrix_matrix_product<
                Index,
                LhsScalar, (ActualLhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(LhsBlasTraits::NeedToConjugate),
                RhsScalar, (ActualRhsTypeCleaned::Flags&RowMajorBit) ? RowMajor : ColMajor, bool(RhsBlasTraits::NeedToConjugate),
                (Dst::Flags&RowMajorBit) ? RowMajor : ColMajor>,
            ActualLhsTypeCleaned, ActualRhsTypeCleaned, Dst, BlockingType> GemmFunctor;
        
        BlockingType blocking(dst.rows(), dst.cols(), lhs.cols(), 1, true);
        internal::parallelize_gemm<(Dst::MaxRowsAtCompileTime>32 || Dst::MaxRowsAtCompileTime==Dynamic)>
            (GemmFunctor(lhs, rhs, dst, alpha, blocking), a_lhs.rows(), a_rhs.cols(), a_lhs.cols(), Dst::Flags&RowMajorBit);
    }
};

/**
   Specialization for a row-major destination matrix => simple
   transposition of the product
*/
template<typename Index,
         typename T, int _rows, int _cols, int _k1, int _k2,
         int LhsStorageOrder, bool ConjugateLhs,
         int RhsStorageOrder, bool ConjugateRhs>
struct general_matrix_matrix_product<Index,
                                     fdbb::ScalarType<T,_rows,_k1>,
                                     LhsStorageOrder, ConjugateLhs,
                                     fdbb::ScalarType<T,_k2,_cols>,
                                     RhsStorageOrder, ConjugateRhs,
                                     RowMajor>
{
    typedef fdbb::ScalarType<T,_rows,_k1> LhsScalar;
    typedef fdbb::ScalarType<T,_k2,_cols> RhsScalar;
    typedef gebp_traits<RhsScalar,LhsScalar> Traits;
            
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;
                
    static EIGEN_STRONG_INLINE void run(Index rows, Index cols, Index depth,
                                        const LhsScalar* lhs, Index lhsStride,
                                        const RhsScalar* rhs, Index rhsStride,
                                        ResScalar* res, Index resStride,
                                        T alpha,
                                        level3_blocking<RhsScalar,LhsScalar>& blocking,
                                        GemmParallelInfo<Index>* info = 0)
    {
        // transpose the product such that the result is column major
        general_matrix_matrix_product<Index,
                                      RhsScalar, RhsStorageOrder==RowMajor ? ColMajor : RowMajor, ConjugateRhs,
                                      LhsScalar, LhsStorageOrder==RowMajor ? ColMajor : RowMajor, ConjugateLhs,
                                      ColMajor>
            ::run(cols,rows,depth,rhs,rhsStride,lhs,lhsStride,res,resStride,alpha,blocking,info);
    }
};

/**
   Specialization for a row-major destination matrix => simple
   transposition of the product
*/
template<typename Index,
         typename T, int _rows, int _cols,
         int LhsStorageOrder, bool ConjugateLhs,
         int RhsStorageOrder, bool ConjugateRhs>
struct general_matrix_matrix_product<Index,
                                     fdbb::ScalarType<T,_rows,_cols>,
                                     LhsStorageOrder, ConjugateLhs,
                                     T,
                                     RhsStorageOrder, ConjugateRhs,
                                     RowMajor>
{
    typedef fdbb::ScalarType<T,_rows,_cols> LhsScalar;
    typedef T RhsScalar;
    typedef gebp_traits<RhsScalar,LhsScalar> Traits;
    
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;
    
    static EIGEN_STRONG_INLINE void run(Index rows, Index cols, Index depth,
                                        const LhsScalar* lhs, Index lhsStride,
                                        const RhsScalar* rhs, Index rhsStride,
                                        ResScalar* res, Index resStride,
                                        T alpha,
                                        level3_blocking<RhsScalar,LhsScalar>& blocking,
                                        GemmParallelInfo<Index>* info = 0)
    {
        // transpose the product such that the result is column major
        general_matrix_matrix_product<Index,
                                      RhsScalar, RhsStorageOrder==RowMajor ? ColMajor : RowMajor, ConjugateRhs,
                                      LhsScalar, LhsStorageOrder==RowMajor ? ColMajor : RowMajor, ConjugateLhs,
                                      ColMajor>
            ::run(cols,rows,depth,rhs,rhsStride,lhs,lhsStride,res,resStride,alpha,blocking,info);
    }
};

/**
   Specialization for a row-major destination matrix => simple
   transposition of the product
*/
template<typename Index,
         typename T, int _rows, int _cols,
         int LhsStorageOrder, bool ConjugateLhs,
         int RhsStorageOrder, bool ConjugateRhs>
struct general_matrix_matrix_product<Index,
                                     T,
                                     LhsStorageOrder, ConjugateLhs,
                                     fdbb::ScalarType<T,_rows,_cols>,
                                     RhsStorageOrder, ConjugateRhs,
                                     RowMajor>
{
    typedef T LhsScalar;
    typedef fdbb::ScalarType<T,_rows,_cols> RhsScalar;
    typedef gebp_traits<RhsScalar,LhsScalar> Traits;
            
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;
                
    static EIGEN_STRONG_INLINE void run(Index rows, Index cols, Index depth,
                                        const LhsScalar* lhs, Index lhsStride,
                                        const RhsScalar* rhs, Index rhsStride,
                                        ResScalar* res, Index resStride,
                                        T alpha,
                                        level3_blocking<RhsScalar,LhsScalar>& blocking,
                                        GemmParallelInfo<Index>* info = 0)
    {
        // transpose the product such that the result is column major
        general_matrix_matrix_product<Index,
                                      RhsScalar, RhsStorageOrder==RowMajor ? ColMajor : RowMajor, ConjugateRhs,
                                      LhsScalar, LhsStorageOrder==RowMajor ? ColMajor : RowMajor, ConjugateLhs,
                                      ColMajor>
            ::run(cols,rows,depth,rhs,rhsStride,lhs,lhsStride,res,resStride,alpha,blocking,info);
    }
};

/**
   Specialization for a col-major destination matrix
   => Blocking algorithm following Goto's paper
*/
template<typename Index,
         typename T, int _rows, int _cols, int _k1, int _k2,
         int LhsStorageOrder, bool ConjugateLhs,
         int RhsStorageOrder, bool ConjugateRhs>
struct general_matrix_matrix_product<Index,
                                     fdbb::ScalarType<T,_rows,_k1>,
                                     LhsStorageOrder, ConjugateLhs,
                                     fdbb::ScalarType<T,_k2,_cols>,
                                     RhsStorageOrder, ConjugateRhs,
                                     ColMajor>
{
    typedef fdbb::ScalarType<T,_rows,_k1> LhsScalar;
    typedef fdbb::ScalarType<T,_k2,_cols> RhsScalar;
    typedef gebp_traits<LhsScalar,RhsScalar> Traits;
    
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;

    static void run(Index rows, Index cols, Index depth,
                    const LhsScalar* _lhs, Index lhsStride,
                    const RhsScalar* _rhs, Index rhsStride,
                    ResScalar* _res, Index resStride,
                    T alpha,
                    level3_blocking<LhsScalar,RhsScalar>& blocking,
                    GemmParallelInfo<Index>* info = 0)
    {
        typedef const_blas_data_mapper<LhsScalar, Index, LhsStorageOrder> LhsMapper;
        typedef const_blas_data_mapper<RhsScalar, Index, RhsStorageOrder> RhsMapper;
        typedef blas_data_mapper<typename Traits::ResScalar, Index, ColMajor> ResMapper;
        LhsMapper lhs(_lhs,lhsStride);
        RhsMapper rhs(_rhs,rhsStride);
        ResMapper res(_res, resStride);
        
        Index kc = blocking.kc();                   // cache block size along the K direction
        Index mc = (std::min)(rows,blocking.mc());  // cache block size along the M direction
        Index nc = (std::min)(cols,blocking.nc());  // cache block size along the N direction
        
        gemm_pack_lhs<LhsScalar, Index, LhsMapper, Traits::mr, Traits::LhsProgress, LhsStorageOrder> pack_lhs;
        gemm_pack_rhs<RhsScalar, Index, RhsMapper, Traits::nr, RhsStorageOrder> pack_rhs;
        gebp_kernel<LhsScalar, RhsScalar, Index, ResMapper, Traits::mr, Traits::nr, ConjugateLhs, ConjugateRhs> gebp;
        
#ifdef EIGEN_HAS_OPENMP
        if(info)
        {
            // this is the parallel version!
            int tid = omp_get_thread_num();
            int threads = omp_get_num_threads();
                
            LhsScalar* blockA = blocking.blockA();
            eigen_internal_assert(blockA!=0);
                
            std::size_t sizeB = kc*nc;
            ei_declare_aligned_stack_constructed_variable(RhsScalar, blockB, sizeB, 0);
                
            // For each horizontal panel of the rhs, and corresponding vertical panel of the lhs...
            for(Index k=0; k<depth; k+=kc)
            {
                const Index actual_kc = (std::min)(k+kc,depth)-k; // => rows of B', and cols of the A'
                        
                // In order to reduce the chance that a thread has to wait for the other,
                // let's start by packing B'.
                pack_rhs(blockB, rhs.getSubMapper(k,0), actual_kc, nc);
                        
                // Pack A_k to A' in a parallel fashion:
                // each thread packs the sub block A_k,i to A'_i where i is the thread id.
                        
                // However, before copying to A'_i, we have to make sure that no other thread is still using it,
                // i.e., we test that info[tid].users equals 0.
                // Then, we set info[tid].users to the number of threads to mark that all other threads are going to use it.
                while(info[tid].users!=0) {}
                info[tid].users += threads;
                        
                pack_lhs(blockA+info[tid].lhs_start*actual_kc, lhs.getSubMapper(info[tid].lhs_start,k), actual_kc, info[tid].lhs_length);
                        
                // Notify the other threads that the part A'_i is ready to go.
                info[tid].sync = k;
                        
                // Computes C_i += A' * B' per A'_i
                for(int shift=0; shift<threads; ++shift)
                {
                    int i = (tid+shift)%threads;
                                
                    // At this point we have to make sure that A'_i has been updated by the thread i,
                    // we use testAndSetOrdered to mimic a volatile access.
                    // However, no need to wait for the B' part which has been updated by the current thread!
                    if (shift>0) {
                        while(info[i].sync!=k) {
                        }
                    }
                                
                    gebp(res.getSubMapper(info[i].lhs_start, 0), blockA+info[i].lhs_start*actual_kc, blockB, info[i].lhs_length, actual_kc, nc, alpha);
                }
                        
                // Then keep going as usual with the remaining B'
                for(Index j=nc; j<cols; j+=nc)
                {
                    const Index actual_nc = (std::min)(j+nc,cols)-j;
                                
                    // pack B_k,j to B'
                    pack_rhs(blockB, rhs.getSubMapper(k,j), actual_kc, actual_nc);
                                
                    // C_j += A' * B'
                    gebp(res.getSubMapper(0, j), blockA, blockB, rows, actual_kc, actual_nc, alpha);
                }
                        
                // Release all the sub blocks A'_i of A' for the current thread,
                // i.e., we simply decrement the number of users by 1
                for(Index i=0; i<threads; ++i)
#pragma omp atomic
                    info[i].users -= 1;
            }
        }
        else
#endif // EIGEN_HAS_OPENMP
        {
            EIGEN_UNUSED_VARIABLE(info);
                
            // this is the sequential version!
            std::size_t sizeA = kc*mc;
            std::size_t sizeB = kc*nc;
                
            ei_declare_aligned_stack_constructed_variable(LhsScalar, blockA, sizeA, blocking.blockA());
            ei_declare_aligned_stack_constructed_variable(RhsScalar, blockB, sizeB, blocking.blockB());
                
            const bool pack_rhs_once = mc!=rows && kc==depth && nc==cols;
                
            // For each horizontal panel of the rhs, and corresponding panel of the lhs...
            for(Index i2=0; i2<rows; i2+=mc)
            {
                const Index actual_mc = (std::min)(i2+mc,rows)-i2;
                        
                for(Index k2=0; k2<depth; k2+=kc)
                {
                    const Index actual_kc = (std::min)(k2+kc,depth)-k2;
                                
                    // OK, here we have selected one horizontal panel of rhs and one vertical panel of lhs.
                    // => Pack lhs's panel into a sequential chunk of memory (L2/L3 caching)
                    // Note that this panel will be read as many times as the number of blocks in the rhs's
                    // horizontal panel which is, in practice, a very low number.
                    pack_lhs(blockA, lhs.getSubMapper(i2,k2), actual_kc, actual_mc);
                                
                    // For each kc x nc block of the rhs's horizontal panel...
                    for(Index j2=0; j2<cols; j2+=nc)
                    {
                        const Index actual_nc = (std::min)(j2+nc,cols)-j2;
                                        
                        // We pack the rhs's block into a sequential chunk of memory (L2 caching)
                        // Note that this block will be read a very high number of times, which is equal to the number of
                        // micro horizontal panel of the large rhs's panel (e.g., rows/12 times).
                        if((!pack_rhs_once) || i2==0)
                            pack_rhs(blockB, rhs.getSubMapper(k2,j2), actual_kc, actual_nc);
                                        
                        // Everything is packed, we can now call the panel * block kernel:
                        gebp(res.getSubMapper(i2, j2), blockA, blockB, actual_mc, actual_kc, actual_nc, alpha);
                    }
                }
            }
        }
    }
};

/**
   Specialization for a col-major destination matrix
   => Blocking algorithm following Goto's paper
*/
template<typename Index,
         typename T, int _rows, int _cols,
         int LhsStorageOrder, bool ConjugateLhs,
         int RhsStorageOrder, bool ConjugateRhs>
struct general_matrix_matrix_product<Index,
                                     fdbb::ScalarType<T,_rows,_cols>,
                                     LhsStorageOrder, ConjugateLhs,
                                     T,
                                     RhsStorageOrder, ConjugateRhs,
                                     ColMajor>
{
    typedef fdbb::ScalarType<T,_rows,_cols> LhsScalar;
    typedef T RhsScalar;
    typedef gebp_traits<LhsScalar,RhsScalar> Traits;
    
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;

    static void run(Index rows, Index cols, Index depth,
                    const LhsScalar* _lhs, Index lhsStride,
                    const RhsScalar* _rhs, Index rhsStride,
                    ResScalar* _res, Index resStride,
                    T alpha,
                    level3_blocking<LhsScalar,RhsScalar>& blocking,
                    GemmParallelInfo<Index>* info = 0)
    {
        typedef const_blas_data_mapper<LhsScalar, Index, LhsStorageOrder> LhsMapper;
        typedef const_blas_data_mapper<RhsScalar, Index, RhsStorageOrder> RhsMapper;
        typedef blas_data_mapper<typename Traits::ResScalar, Index, ColMajor> ResMapper;
        LhsMapper lhs(_lhs,lhsStride);
        RhsMapper rhs(_rhs,rhsStride);
        ResMapper res(_res, resStride);
        
        Index kc = blocking.kc();                   // cache block size along the K direction
        Index mc = (std::min)(rows,blocking.mc());  // cache block size along the M direction
        Index nc = (std::min)(cols,blocking.nc());  // cache block size along the N direction
        
        gemm_pack_lhs<LhsScalar, Index, LhsMapper, Traits::mr, Traits::LhsProgress, LhsStorageOrder> pack_lhs;
        gemm_pack_rhs<RhsScalar, Index, RhsMapper, Traits::nr, RhsStorageOrder> pack_rhs;
        gebp_kernel<LhsScalar, RhsScalar, Index, ResMapper, Traits::mr, Traits::nr, ConjugateLhs, ConjugateRhs> gebp;
        
#ifdef EIGEN_HAS_OPENMP
        if(info)
        {
            // this is the parallel version!
            int tid = omp_get_thread_num();
            int threads = omp_get_num_threads();
                
            LhsScalar* blockA = blocking.blockA();
            eigen_internal_assert(blockA!=0);
                
            std::size_t sizeB = kc*nc;
            ei_declare_aligned_stack_constructed_variable(RhsScalar, blockB, sizeB, 0);
                
            // For each horizontal panel of the rhs, and corresponding vertical panel of the lhs...
            for(Index k=0; k<depth; k+=kc)
            {
                const Index actual_kc = (std::min)(k+kc,depth)-k; // => rows of B', and cols of the A'
                        
                // In order to reduce the chance that a thread has to wait for the other,
                // let's start by packing B'.
                pack_rhs(blockB, rhs.getSubMapper(k,0), actual_kc, nc);
                        
                // Pack A_k to A' in a parallel fashion:
                // each thread packs the sub block A_k,i to A'_i where i is the thread id.
                        
                // However, before copying to A'_i, we have to make sure that no other thread is still using it,
                // i.e., we test that info[tid].users equals 0.
                // Then, we set info[tid].users to the number of threads to mark that all other threads are going to use it.
                while(info[tid].users!=0) {}
                info[tid].users += threads;
                        
                pack_lhs(blockA+info[tid].lhs_start*actual_kc, lhs.getSubMapper(info[tid].lhs_start,k), actual_kc, info[tid].lhs_length);
                        
                // Notify the other threads that the part A'_i is ready to go.
                info[tid].sync = k;
                        
                // Computes C_i += A' * B' per A'_i
                for(int shift=0; shift<threads; ++shift)
                {
                    int i = (tid+shift)%threads;
                                
                    // At this point we have to make sure that A'_i has been updated by the thread i,
                    // we use testAndSetOrdered to mimic a volatile access.
                    // However, no need to wait for the B' part which has been updated by the current thread!
                    if (shift>0) {
                        while(info[i].sync!=k) {
                        }
                    }
                                
                    gebp(res.getSubMapper(info[i].lhs_start, 0), blockA+info[i].lhs_start*actual_kc, blockB, info[i].lhs_length, actual_kc, nc, alpha);
                }
                        
                // Then keep going as usual with the remaining B'
                for(Index j=nc; j<cols; j+=nc)
                {
                    const Index actual_nc = (std::min)(j+nc,cols)-j;
                                
                    // pack B_k,j to B'
                    pack_rhs(blockB, rhs.getSubMapper(k,j), actual_kc, actual_nc);
                                
                    // C_j += A' * B'
                    gebp(res.getSubMapper(0, j), blockA, blockB, rows, actual_kc, actual_nc, alpha);
                }
                        
                // Release all the sub blocks A'_i of A' for the current thread,
                // i.e., we simply decrement the number of users by 1
                for(Index i=0; i<threads; ++i)
#pragma omp atomic
                    info[i].users -= 1;
            }
        }
        else
#endif // EIGEN_HAS_OPENMP
        {
            EIGEN_UNUSED_VARIABLE(info);
                
            // this is the sequential version!
            std::size_t sizeA = kc*mc;
            std::size_t sizeB = kc*nc;
                
            ei_declare_aligned_stack_constructed_variable(LhsScalar, blockA, sizeA, blocking.blockA());
            ei_declare_aligned_stack_constructed_variable(RhsScalar, blockB, sizeB, blocking.blockB());
                
            const bool pack_rhs_once = mc!=rows && kc==depth && nc==cols;
                
            // For each horizontal panel of the rhs, and corresponding panel of the lhs...
            for(Index i2=0; i2<rows; i2+=mc)
            {
                const Index actual_mc = (std::min)(i2+mc,rows)-i2;
                        
                for(Index k2=0; k2<depth; k2+=kc)
                {
                    const Index actual_kc = (std::min)(k2+kc,depth)-k2;
                                
                    // OK, here we have selected one horizontal panel of rhs and one vertical panel of lhs.
                    // => Pack lhs's panel into a sequential chunk of memory (L2/L3 caching)
                    // Note that this panel will be read as many times as the number of blocks in the rhs's
                    // horizontal panel which is, in practice, a very low number.
                    pack_lhs(blockA, lhs.getSubMapper(i2,k2), actual_kc, actual_mc);
                                
                    // For each kc x nc block of the rhs's horizontal panel...
                    for(Index j2=0; j2<cols; j2+=nc)
                    {
                        const Index actual_nc = (std::min)(j2+nc,cols)-j2;
                                        
                        // We pack the rhs's block into a sequential chunk of memory (L2 caching)
                        // Note that this block will be read a very high number of times, which is equal to the number of
                        // micro horizontal panel of the large rhs's panel (e.g., rows/12 times).
                        if((!pack_rhs_once) || i2==0)
                            pack_rhs(blockB, rhs.getSubMapper(k2,j2), actual_kc, actual_nc);
                                        
                        // Everything is packed, we can now call the panel * block kernel:
                        gebp(res.getSubMapper(i2, j2), blockA, blockB, actual_mc, actual_kc, actual_nc, alpha);
                    }
                }
            }
        }
    }
};

/**
   Specialization for a col-major destination matrix
   => Blocking algorithm following Goto's paper
*/
template<typename Index,
         typename T, int _rows, int _cols,
         int LhsStorageOrder, bool ConjugateLhs,
         int RhsStorageOrder, bool ConjugateRhs>
struct general_matrix_matrix_product<Index,
                                     T,
                                     LhsStorageOrder, ConjugateLhs,
                                     fdbb::ScalarType<T,_rows,_cols>,
                                     RhsStorageOrder, ConjugateRhs,
                                     ColMajor>
{
    typedef T LhsScalar;
    typedef fdbb::ScalarType<T,_rows,_cols> RhsScalar;
    typedef gebp_traits<LhsScalar,RhsScalar> Traits;
    
    typedef typename ScalarBinaryOpTraits<LhsScalar, RhsScalar>::ReturnType ResScalar;

    static void run(Index rows, Index cols, Index depth,
                    const LhsScalar* _lhs, Index lhsStride,
                    const RhsScalar* _rhs, Index rhsStride,
                    ResScalar* _res, Index resStride,
                    T alpha,
                    level3_blocking<LhsScalar,RhsScalar>& blocking,
                    GemmParallelInfo<Index>* info = 0)
    {
        typedef const_blas_data_mapper<LhsScalar, Index, LhsStorageOrder> LhsMapper;
        typedef const_blas_data_mapper<RhsScalar, Index, RhsStorageOrder> RhsMapper;
        typedef blas_data_mapper<typename Traits::ResScalar, Index, ColMajor> ResMapper;
        LhsMapper lhs(_lhs,lhsStride);
        RhsMapper rhs(_rhs,rhsStride);
        ResMapper res(_res, resStride);
        
        Index kc = blocking.kc();                   // cache block size along the K direction
        Index mc = (std::min)(rows,blocking.mc());  // cache block size along the M direction
        Index nc = (std::min)(cols,blocking.nc());  // cache block size along the N direction
        
        gemm_pack_lhs<LhsScalar, Index, LhsMapper, Traits::mr, Traits::LhsProgress, LhsStorageOrder> pack_lhs;
        gemm_pack_rhs<RhsScalar, Index, RhsMapper, Traits::nr, RhsStorageOrder> pack_rhs;
        gebp_kernel<LhsScalar, RhsScalar, Index, ResMapper, Traits::mr, Traits::nr, ConjugateLhs, ConjugateRhs> gebp;
        
#ifdef EIGEN_HAS_OPENMP
        if(info)
        {
            // this is the parallel version!
            int tid = omp_get_thread_num();
            int threads = omp_get_num_threads();
                
            LhsScalar* blockA = blocking.blockA();
            eigen_internal_assert(blockA!=0);
                
            std::size_t sizeB = kc*nc;
            ei_declare_aligned_stack_constructed_variable(RhsScalar, blockB, sizeB, 0);
                
            // For each horizontal panel of the rhs, and corresponding vertical panel of the lhs...
            for(Index k=0; k<depth; k+=kc)
            {
                const Index actual_kc = (std::min)(k+kc,depth)-k; // => rows of B', and cols of the A'
                        
                // In order to reduce the chance that a thread has to wait for the other,
                // let's start by packing B'.
                pack_rhs(blockB, rhs.getSubMapper(k,0), actual_kc, nc);
                        
                // Pack A_k to A' in a parallel fashion:
                // each thread packs the sub block A_k,i to A'_i where i is the thread id.
                        
                // However, before copying to A'_i, we have to make sure that no other thread is still using it,
                // i.e., we test that info[tid].users equals 0.
                // Then, we set info[tid].users to the number of threads to mark that all other threads are going to use it.
                while(info[tid].users!=0) {}
                info[tid].users += threads;
                        
                pack_lhs(blockA+info[tid].lhs_start*actual_kc, lhs.getSubMapper(info[tid].lhs_start,k), actual_kc, info[tid].lhs_length);
                        
                // Notify the other threads that the part A'_i is ready to go.
                info[tid].sync = k;
                        
                // Computes C_i += A' * B' per A'_i
                for(int shift=0; shift<threads; ++shift)
                {
                    int i = (tid+shift)%threads;
                                
                    // At this point we have to make sure that A'_i has been updated by the thread i,
                    // we use testAndSetOrdered to mimic a volatile access.
                    // However, no need to wait for the B' part which has been updated by the current thread!
                    if (shift>0) {
                        while(info[i].sync!=k) {
                        }
                    }
                                
                    gebp(res.getSubMapper(info[i].lhs_start, 0), blockA+info[i].lhs_start*actual_kc, blockB, info[i].lhs_length, actual_kc, nc, alpha);
                }
                        
                // Then keep going as usual with the remaining B'
                for(Index j=nc; j<cols; j+=nc)
                {
                    const Index actual_nc = (std::min)(j+nc,cols)-j;
                                
                    // pack B_k,j to B'
                    pack_rhs(blockB, rhs.getSubMapper(k,j), actual_kc, actual_nc);
                                
                    // C_j += A' * B'
                    gebp(res.getSubMapper(0, j), blockA, blockB, rows, actual_kc, actual_nc, alpha);
                }
                        
                // Release all the sub blocks A'_i of A' for the current thread,
                // i.e., we simply decrement the number of users by 1
                for(Index i=0; i<threads; ++i)
#pragma omp atomic
                    info[i].users -= 1;
            }
        }
        else
#endif // EIGEN_HAS_OPENMP
        {
            EIGEN_UNUSED_VARIABLE(info);
                
            // this is the sequential version!
            std::size_t sizeA = kc*mc;
            std::size_t sizeB = kc*nc;
                
            ei_declare_aligned_stack_constructed_variable(LhsScalar, blockA, sizeA, blocking.blockA());
            ei_declare_aligned_stack_constructed_variable(RhsScalar, blockB, sizeB, blocking.blockB());
                
            const bool pack_rhs_once = mc!=rows && kc==depth && nc==cols;
                
            // For each horizontal panel of the rhs, and corresponding panel of the lhs...
            for(Index i2=0; i2<rows; i2+=mc)
            {
                const Index actual_mc = (std::min)(i2+mc,rows)-i2;
                        
                for(Index k2=0; k2<depth; k2+=kc)
                {
                    const Index actual_kc = (std::min)(k2+kc,depth)-k2;
                                
                    // OK, here we have selected one horizontal panel of rhs and one vertical panel of lhs.
                    // => Pack lhs's panel into a sequential chunk of memory (L2/L3 caching)
                    // Note that this panel will be read as many times as the number of blocks in the rhs's
                    // horizontal panel which is, in practice, a very low number.
                    pack_lhs(blockA, lhs.getSubMapper(i2,k2), actual_kc, actual_mc);
                                
                    // For each kc x nc block of the rhs's horizontal panel...
                    for(Index j2=0; j2<cols; j2+=nc)
                    {
                        const Index actual_nc = (std::min)(j2+nc,cols)-j2;
                                        
                        // We pack the rhs's block into a sequential chunk of memory (L2 caching)
                        // Note that this block will be read a very high number of times, which is equal to the number of
                        // micro horizontal panel of the large rhs's panel (e.g., rows/12 times).
                        if((!pack_rhs_once) || i2==0)
                            pack_rhs(blockB, rhs.getSubMapper(k2,j2), actual_kc, actual_nc);
                                        
                        // Everything is packed, we can now call the panel * block kernel:
                        gebp(res.getSubMapper(i2, j2), blockA, blockB, actual_mc, actual_kc, actual_nc, alpha);
                    }
                }
            }
        }
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   sparse Eigen matrix with intrinsic scalar type and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,rows,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         int ProductType>
struct generic_product_impl<SparseMatrix<T, _Options1, _StorageIndex1>,
                            Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<SparseMatrix<T, _Options1, _StorageIndex1>,
                                Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<SparseMatrix<T, _Options1, _StorageIndex1>,
                                                     Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef SparseMatrix<T, _Options1, _StorageIndex1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and a dense Eigen matrix with
   intrinsic scalar type
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         int ProductType>
struct generic_product_impl<SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options1, _StorageIndex1>,
                            Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options1, _StorageIndex1>,
                                Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options1, _StorageIndex1>,
                                                     Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options1, _StorageIndex1> Lhs;
    typedef Matrix<T, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and a dense Eigen matrix with custom
   scalar type fdbb::ScalarType<T,k2,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         int ProductType>
struct generic_product_impl<SparseMatrix<fdbb::ScalarType<T,_rows,_k1>, _Options1, _StorageIndex1>,
                            Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T,_rows,_k1>, _Options1, _StorageIndex1>,
                                Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<SparseMatrix<fdbb::ScalarType<T,_rows,_k1>, _Options1, _StorageIndex1>,
                                                     Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef SparseMatrix<fdbb::ScalarType<T,_rows,_k1>, _Options1, _StorageIndex1> Lhs;
    typedef Matrix<fdbb::ScalarType<T,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
    
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with intrinsic scalar
   type as XprType and a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols>.
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const SparseMatrix<T1, _Options1, _StorageIndex1>
                                         >,
                            Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const SparseMatrix<T1, _Options1, _StorageIndex1>
                                             >,
                                Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const SparseMatrix<T1, _Options1, _StorageIndex1>
                                                                  >,
                                                     Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<T1, _Options1, _StorageIndex1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and a dense Eigen
   matrix with intrinsic scalar type
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                         >,
                            Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                             >,
                                Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                                                  >,
                                                     Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1> > Lhs;
    typedef Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,k2,cols>.
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                         >,
                            Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                             >,
                                Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                                                  >,
                                                     Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1> > Lhs;
    typedef Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   sparse Eigen matrix with intrinsic scalar type and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<SparseMatrix<T1, _Options1, _StorageIndex1>,
                            CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<SparseMatrix<T1, _Options1, _StorageIndex1>,
                                CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<SparseMatrix<T1, _Options1, _StorageIndex1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef SparseMatrix<T1, _Options1, _StorageIndex1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and an Eigen CwiseUnaryOp with a
   dense Eigen matrix with intrinsic scalar type as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>,
                            CwiseUnaryOp<UnaryOp,
                                         const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>,
                                CwiseUnaryOp<UnaryOp,
                                             const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and an Eigen CwiseUnaryOp with a dense
   Eigen matrix with custom scalar type fdbb::ScalarType<T,k2,cols> as
   XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>,
                            CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>,
                                CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

//------------------------------------------------------------------------------
// EIGEN: DENSE-MATRIX x SPARSE-MATRIX MULTIPLY
//------------------------------------------------------------------------------

template<typename SparseLhsType, typename DenseRhsType, typename DenseResType,
         typename AlphaType,
         int LhsStorageOrder = ((SparseLhsType::Flags&RowMajorBit)==RowMajorBit) ? RowMajor : ColMajor,
         bool ColPerCol = ((DenseRhsType::Flags&RowMajorBit)==0) || DenseRhsType::ColsAtCompileTime==1>
struct dense_time_sparse_product_impl;

template<typename SparseLhsType, typename DenseRhsType, typename DenseResType, typename AlphaType>
struct dense_time_sparse_product_impl<SparseLhsType,DenseRhsType,DenseResType,AlphaType, RowMajor, true>
{
  typedef typename internal::remove_all<SparseLhsType>::type Lhs;
  typedef typename internal::remove_all<DenseRhsType>::type Rhs;
  typedef typename internal::remove_all<DenseResType>::type Res;
  typedef typename evaluator<Lhs>::InnerIterator LhsInnerIterator;
  typedef evaluator<Lhs> LhsEval;
  static void run(const SparseLhsType& lhs, const DenseRhsType& rhs, DenseResType& res, const AlphaType& alpha)
  {
    LhsEval lhsEval(lhs);
    
    Index n = lhs.outerSize();
#ifdef EIGEN_HAS_OPENMP
    Eigen::initParallel();
    Index threads = Eigen::nbThreads();
#endif
    
    for(Index c=0; c<rhs.cols(); ++c)
    {
#ifdef EIGEN_HAS_OPENMP
      // This 20000 threshold has been found experimentally on 2D and 3D Poisson problems.
      // It basically represents the minimal amount of work to be done to be worth it.
      if(threads>1 && lhsEval.nonZerosEstimate() > 20000)
      {
        #pragma omp parallel for schedule(dynamic,(n+threads*4-1)/(threads*4)) num_threads(threads)
        for(Index i=0; i<n; ++i)
          processRow(lhsEval,rhs,res,alpha,i,c);
      }
      else
#endif
      {
        for(Index i=0; i<n; ++i)
          processRow(lhsEval,rhs,res,alpha,i,c);
      }
    }
  }
  
  static void processRow(const LhsEval& lhsEval, const DenseRhsType& rhs, DenseResType& res, const AlphaType& alpha, Index i, Index col)
  {
    typename Res::Scalar tmp(0);
    for(LhsInnerIterator it(lhsEval,i); it ;++it)
      tmp += rhs.coeff(it.index(),col) * it.value();
    res.coeffRef(i,col) += alpha * tmp;
  }
  
};

// FIXME: what is the purpose of the following specialization? Is it for the BlockedSparse format?
// -> let's disable it for now as it is conflicting with generic scalar*matrix and matrix*scalar operators
// template<typename T1, typename T2/*, int _Options, typename _StrideType*/>
// struct ScalarBinaryOpTraits<T1, Ref<T2/*, _Options, _StrideType*/> >
// {
//   enum {
//     Defined = 1
//   };
//   typedef typename CwiseUnaryOp<scalar_multiple2_op<T1, typename T2::Scalar>, T2>::PlainObject ReturnType;
// };

template<typename SparseLhsType, typename DenseRhsType, typename DenseResType, typename AlphaType>
struct dense_time_sparse_product_impl<SparseLhsType,DenseRhsType,DenseResType,AlphaType, ColMajor, true>
{
  typedef typename internal::remove_all<SparseLhsType>::type Lhs;
  typedef typename internal::remove_all<DenseRhsType>::type Rhs;
  typedef typename internal::remove_all<DenseResType>::type Res;
  typedef typename evaluator<Lhs>::InnerIterator LhsInnerIterator;
  static void run(const SparseLhsType& lhs, const DenseRhsType& rhs, DenseResType& res, const AlphaType& alpha)
  {
    evaluator<Lhs> lhsEval(lhs);
    for(Index c=0; c<rhs.cols(); ++c)
    {
      for(Index j=0; j<lhs.outerSize(); ++j)
      {
//        typename Res::Scalar rhs_j = alpha * rhs.coeff(j,c);
        typename ScalarBinaryOpTraits<AlphaType, typename Rhs::Scalar>::ReturnType rhs_j(alpha * rhs.coeff(j,c));
        for(LhsInnerIterator it(lhsEval,j); it ;++it)
          res.coeffRef(it.index(),c) += rhs_j * it.value();
      }
    }
  }
};

template<typename SparseLhsType, typename DenseRhsType, typename DenseResType, typename AlphaType>
struct dense_time_sparse_product_impl<SparseLhsType,DenseRhsType,DenseResType,AlphaType, RowMajor, false>
{
  typedef typename internal::remove_all<SparseLhsType>::type Lhs;
  typedef typename internal::remove_all<DenseRhsType>::type Rhs;
  typedef typename internal::remove_all<DenseResType>::type Res;
  typedef typename evaluator<Lhs>::InnerIterator LhsInnerIterator;
  static void run(const SparseLhsType& lhs, const DenseRhsType& rhs, DenseResType& res, const AlphaType& alpha)
  {
    evaluator<Lhs> lhsEval(lhs);
    for(Index j=0; j<lhs.outerSize(); ++j)
    {
      typename Res::RowXpr res_j(res.row(j));
      for(LhsInnerIterator it(lhsEval,j); it ;++it)
          res_j += (alpha*rhs.row(it.index())) * it.value();
    }
  }
};

template<typename SparseLhsType, typename DenseRhsType, typename DenseResType, typename AlphaType>
struct dense_time_sparse_product_impl<SparseLhsType,DenseRhsType,DenseResType,AlphaType, ColMajor, false>
{
  typedef typename internal::remove_all<SparseLhsType>::type Lhs;
  typedef typename internal::remove_all<DenseRhsType>::type Rhs;
  typedef typename internal::remove_all<DenseResType>::type Res;
  typedef typename evaluator<Lhs>::InnerIterator LhsInnerIterator;
  static void run(const SparseLhsType& lhs, const DenseRhsType& rhs, DenseResType& res, const AlphaType& alpha)
  {
    evaluator<Lhs> lhsEval(lhs);
    for(Index j=0; j<lhs.outerSize(); ++j)
    {
      typename Rhs::ConstRowXpr rhs_j(rhs.row(j));
      for(LhsInnerIterator it(lhsEval,j); it ;++it)
          res.row(it.index()) += (alpha*rhs_j) * it.value();
    }
  }
};

template<typename SparseLhsType, typename DenseRhsType, typename DenseResType, typename AlphaType>
inline void dense_time_sparse_product(const SparseLhsType& lhs, const DenseRhsType& rhs, DenseResType& res, const AlphaType& alpha)
{
  dense_time_sparse_product_impl<SparseLhsType,DenseRhsType,DenseResType, AlphaType>::run(lhs, rhs, res, alpha);
}

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with intrinsic scalar type and a sparse Eigen
   matrix with custom scalar type fdbb::ScalarType<T,rows,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         int ProductType>
struct generic_product_impl<Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options2, _StorageIndex2>,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options2, _StorageIndex2>,
                                generic_product_impl<Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options2, _StorageIndex2>,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef Matrix<T, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T,_rows,_cols>, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and a sparse Eigen matrix with
   intrinsic scalar type
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         int ProductType>
struct generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            SparseMatrix<T, _Options2, _StorageIndex2>,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                SparseMatrix<T, _Options2, _StorageIndex2>,
                                generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     SparseMatrix<T, _Options2, _StorageIndex2>,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef SparseMatrix<T, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and a sparse Eigen matrix with
   custom scalar type fdbb::ScalarType<T,k2,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         int ProductType>
struct generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            SparseMatrix<fdbb::ScalarType<T,_k2,_cols>, _Options2, _StorageIndex2>,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                SparseMatrix<fdbb::ScalarType<T,_k2,_cols>, _Options2, _StorageIndex2>,
                                generic_product_impl<Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     SparseMatrix<fdbb::ScalarType<T,_k2,_cols>, _Options2, _StorageIndex2>,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef Matrix<fdbb::ScalarType<T,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T,_k2,_cols>, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with intrinsic
   scalar type as XprType and a sparse Eigen matrix with custom
   scalar type fdbb::ScalarType<T,rows,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and a sparse Eigen
   matrix with intrinsic scalar type
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            SparseMatrix<T2, _Options2, _StorageIndex2>,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                SparseMatrix<T2, _Options2, _StorageIndex2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     SparseMatrix<T2, _Options2, _StorageIndex2>,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef SparseMatrix<T2, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and a sparse Eigen
   matrix with custom scalar type fdbb::ScalarType<T,k2,cols>
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>,
                                generic_product_impl<CwiseUnaryOp<UnaryOp,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp, const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2> Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with intrinsic scalar type and an Eigen
   CwiseUnaryOp with a sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            CwiseUnaryOp<UnaryOp,
                                         const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                         >,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                CwiseUnaryOp<UnaryOp,
                                             const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                             >,                                
                                generic_product_impl<Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                                                  >,                                                    
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,cols> and an Eigen CwiseUnaryOp with a
   sparse Eigen matrix with intrinsic scalar type as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            CwiseUnaryOp<UnaryOp,
                                         const SparseMatrix<T2, _Options2, _StorageIndex2>
                                         >,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                CwiseUnaryOp<UnaryOp,
                                             const SparseMatrix<T2, _Options2, _StorageIndex2>
                                             >,                                
                                generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const SparseMatrix<T2, _Options2, _StorageIndex2>
                                                                  >,                                                    
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<T2, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for a
   dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,rows,k1> and an Eigen CwiseUnaryOp with
   sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,k2,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp, int ProductType>
struct generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                            CwiseUnaryOp<UnaryOp,
                                         const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                         >,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                CwiseUnaryOp<UnaryOp,
                                             const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                             >,                                
                                generic_product_impl<Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>,
                                                     CwiseUnaryOp<UnaryOp,
                                                                  const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                                                  >,                                                    
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> Lhs;
    typedef CwiseUnaryOp<UnaryOp, const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T1& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar
   type as XprType and an Eigen CwiseUnaryOp with a sparse Eigen matrix
   with custom scalar type fdbb::ScalarType<T,rows,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp1, typename UnaryOp2, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                        const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                        >,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2>
                                                                  >,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<T1, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const SparseMatrix<fdbb::ScalarType<T2,_rows,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and an Eigen UnaryOp
   with a sparse Eigen matrix with intrinsic scalar type as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp1, typename UnaryOp2, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const SparseMatrix<T2, _Options2, _StorageIndex2>
                                         >,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const SparseMatrix<T2, _Options2, _StorageIndex2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const SparseMatrix<T2, _Options2, _StorageIndex2>
                                                                  >,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_cols>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const SparseMatrix<T2, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a dense Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and an Eigen
   CwiseUnaryOp with a sparse Eigen matrix with custom scalar type
   fdbb::ScalarType<T,k2,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Rows1, int _Cols1, int _Options1, int _MaxRows1, int _MaxCols1,
         int _Options2, typename _StorageIndex2,
         typename UnaryOp1, typename UnaryOp2, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                         >,
                            DenseShape, SparseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2>
                                                                  >,
                                                     DenseShape, SparseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp1, const Matrix<fdbb::ScalarType<T1,_rows,_k1>, _Rows1, _Cols1, _Options1, _MaxRows1, _MaxCols1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const SparseMatrix<fdbb::ScalarType<T2,_k2,_cols>, _Options2, _StorageIndex2> > Rhs;
    
    template<typename Dst>
    static void scaleAndAddTo(Dst& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? Dynamic : 1>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==RowMajorBit) ? 1 : Lhs::RowsAtCompileTime>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        
        // transpose everything
        Transpose<Dst> dstT(dst);
        internal::dense_time_sparse_product(rhsNested.transpose(), lhsNested.transpose(), dstT, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with intrinsic scalar
   type as XprType and an Eigen CwiseUnaryOp with a a dense Eigen
   matrix with custom scalar type fdbb::ScalarType<T,rows,cols> as
   XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const SparseMatrix<T1, _Options1, _StorageIndex1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const SparseMatrix<T1, _Options1, _StorageIndex1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const SparseMatrix<T1, _Options1, _StorageIndex1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp1, const SparseMatrix<T1, _Options1, _StorageIndex1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_rows,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,cols> as XprType and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with intrinsic scalar type
   as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp1, const SparseMatrix<fdbb::ScalarType<T1,_rows,_cols>, _Options1, _StorageIndex1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<T2, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

/**
   Specialized generic matrix-matrix product implementation for an
   Eigen CwiseUnaryOp with a sparse Eigen matrix with custom scalar
   type fdbb::ScalarType<T,rows,k1> as XprType and an Eigen
   CwiseUnaryOp with a dense Eigen matrix with custom scalar type
   fdbb::ScalarType<T,k2,cols> as XprType
   
   This specialization of the generic matrix-matrix product
   implementation is an adaption of the implementation implemented in
   file Eigen/src/Core/products/GeneralMatrixMatrix.h with the major
   change that the scalar multiplicative factor alpha is not of type
   ScalarType<T,rows,cols> but of arithmetic type T. This prevents
   multiplication by an invalid multiplicative factor.
*/
template<typename T1, typename T2, int _rows, int _cols, int _k1, int _k2,
         int _Options1, typename _StorageIndex1,
         int _Rows2, int _Cols2, int _Options2, int _MaxRows2, int _MaxCols2,
         typename UnaryOp1, typename UnaryOp2, int ProductType>
struct generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                         const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                         >,
                            CwiseUnaryOp<UnaryOp2,
                                         const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                         >,
                            SparseShape, DenseShape, ProductType>
    : generic_product_impl_base<CwiseUnaryOp<UnaryOp1,
                                             const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                             >,
                                CwiseUnaryOp<UnaryOp2,
                                             const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                             >,
                                generic_product_impl<CwiseUnaryOp<UnaryOp1,
                                                                  const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1>
                                                                  >,
                                                     CwiseUnaryOp<UnaryOp2,
                                                                  const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2>
                                                                  >,
                                                     SparseShape, DenseShape, ProductType> >
{
    typedef CwiseUnaryOp<UnaryOp1, const SparseMatrix<fdbb::ScalarType<T1,_rows,_k1>, _Options1, _StorageIndex1> > Lhs;
    typedef CwiseUnaryOp<UnaryOp2, const Matrix<fdbb::ScalarType<T2,_k2,_cols>, _Rows2, _Cols2, _Options2, _MaxRows2, _MaxCols2> > Rhs;
        
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const Lhs& lhs, const Rhs& rhs, const T2& alpha)
    {
        typedef typename nested_eval<Lhs,((Rhs::Flags&RowMajorBit)==0) ? 1 : Rhs::ColsAtCompileTime>::type LhsNested;
        typedef typename nested_eval<Rhs,((Lhs::Flags&RowMajorBit)==0) ? 1 : Dynamic>::type RhsNested;
        LhsNested lhsNested(lhs);
        RhsNested rhsNested(rhs);
        internal::sparse_time_dense_product(lhsNested, rhsNested, dst, alpha);
    }
};

} // namespace internal

} // namespace Eigen
#endif

#endif // SCALAR_TYPE_HPP
