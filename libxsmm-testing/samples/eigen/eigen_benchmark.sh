#!/bin/bash

if [[ ! $BUILDLOG ]];
then
    BUILDLOG=build.log
fi

if [[ ! $RUNLOG ]];
then
    RUNLOG=run.log
fi

if [[ ! $NPAR ]];
then
    NPAR=4
fi

rm -f ${BUILDLOG}
rm -f ${RUNLOG}

# >>> Set N,M,K <<<
if [[ ! $N ]] || [[ ! $M ]] || [[ ! $K ]];
then
    NMK=('N=(1 5)' 'M=(10 5)' 'K=(5 5)')
    for elt in "${NMK[@]}"; do eval $elt; done
fi
NMK_LEN=${#M[@]}

# >>> Set n,m,k <<<
if [[ ! $n ]] || [[ ! $m ]] || [[ ! $k ]];
then
    nmk=('n=(1 5)' 'm=(5 5)'  'k=(3 5)')
    for elt in "${nmk[@]}"; do eval $elt; done
fi
nmk_LEN=${#m[@]}


# >>> BLAZE <<<

if [ $BLAZE ];
then
    if [[ ! $A ]] || [[ ! $B ]] || [[ ! $C ]];
    then
        FORMAT="STATIC DYNAMIC HYBRID SPARSE"
        FMT_A="A=("
        FMT_B="B=("
        FMT_C="C=("
        
        # Generate all possible combinations
        for A in $FORMAT;
        do for B in $FORMAT;
           do for C in $FORMAT;
              do 
                  FMT_A="$FMT_A $A"
                  FMT_B="$FMT_B $B"
                  FMT_C="$FMT_C $C"
              done 
           done 
        done

        FMT_A="$FMT_A )"
        FMT_B="$FMT_B )"
        FMT_C="$FMT_C )"
        for elt in "${FMT_A[@]}"; do eval $elt; done
        for elt in "${FMT_B[@]}"; do eval $elt; done
        for elt in "${FMT_C[@]}"; do eval $elt; done
    fi
    FMT_LEN=${#A[@]}

    for XSMM in "1"; #"0" "1";
    do
        for (( F=1; F<${FMT_LEN}+1; F++ ));
        do
            for TYPE in "float";# "double";
            do
                for (( I=1; I<${NMK_LEN}+1; I++ ));
                do
                    for (( i=1; i<${nmk_LEN}+1; i++ ));
                    do
                        touch eigen_benchmark.cpp
                        make eigen_benchmark BLAZE=1 EIGEN=0 \
                             XSMM=${XSMM} \
                             TYPE=${TYPE} \
                             ${A[$F-1]}_A=1 ${B[$F-1]}_B=1 ${C[$F-1]}_C=1 \
                             N=${N[$I-1]} M=${M[$I-1]} K=${K[$I-1]} \
                             m=${m[$i-1]} n=${n[$i-1]} k=${k[$i-1]} \
                             >> ${BUILDLOG} 2>&1 \
                            && ./eigen_test >> ${RUNLOG} 2>&1 \
                                || echo "XSMM=${XSMM} ${A[$F-1]}_A=1 ${B[$F-1]}_B=1 ${C[$F-1]}_C=1 N=${N[$I-1]} M=${M[$I-1]} K=${K[$I-1]} m=${m[$i-1]} n=${n[$i-1]} k=${k[$i-1]} BLAZE failed!!! " >> ${RUNLOG} 2>&1 &
                    done
                done
            done
        done 
    done
fi

# >>> EIGEN <<<

if [ $EIGEN ];
then
    if [[ ! $A ]] || [[ ! $B ]] || [[ ! $C ]];
    then
        FORMAT="STATIC DYNAMIC SPARSE"
        FMT_A="A=("
        FMT_B="B=("
        FMT_C="C=("
        
        # Generate all possible combinations
        for A in $FORMAT;
        do for B in $FORMAT;
           do for C in $FORMAT;
              do 
                  FMT_A="$FMT_A $A"
                  FMT_B="$FMT_B $B"
                  FMT_C="$FMT_C $C"
              done 
           done 
        done
        
        FMT_A="$FMT_A )"
        FMT_B="$FMT_B )"
        FMT_C="$FMT_C )"
        for elt in "${FMT_A[@]}"; do eval $elt; done
        for elt in "${FMT_B[@]}"; do eval $elt; done
        for elt in "${FMT_C[@]}"; do eval $elt; done

        FMT_LEN=${#A[@]}
        for (( F=1; F<${FMT_LEN}+1; F++ ));
        do
            if [ "${A[$F-1]}" != "SPARSE" ] || \
               [ "${B[$F-1]}" != "SPARSE" ] && \
               [ "${C[$F-1]}" == "SPARSE" ];
            then
                unset A[$F-1]
                unset B[$F-1]
                unset C[$F-1]
            fi
        done
        A=( "${A[@]}" )
        B=( "${B[@]}" )
        C=( "${C[@]}" )
    fi
    FMT_LEN=${#A[@]}

    for XSMM in "0" "1";
    do
        for (( F=1; F<${FMT_LEN}+1; F++ ));
        do
            for TYPE in "float" "double";
            do
                for (( I=1; I<${NMK_LEN}+1; I++ ));
                do
                    for (( i=1; i<${nmk_LEN}+1; i++ ));
                    do
                        touch eigen_benchmark.cpp
                        make eigen_benchmark BLAZE=0 EIGEN=1 \
                             XSMM=${XSMM} \
                             TYPE=${TYPE} \
                             ${A[$F-1]}_A=1 ${B[$F-1]}_B=1 ${C[$F-1]}_C=1 \
                             N=${N[$I-1]} M=${M[$I-1]} K=${K[$I-1]} \
                             m=${m[$i-1]} n=${n[$i-1]} k=${k[$i-1]} \
                             >> ${BUILDLOG} 2>&1 \
                            && ./eigen_test >> ${RUNLOG} 2>&1 \
                            || echo "XSMM=${XSMM} ${A[$F-1]}_A=1 ${B[$F-1]}_B=1 ${C[$F-1]}_C=1 N=${N[$I-1]} M=${M[$I-1]} K=${K[$I-1]} m=${m[$i-1]} n=${n[$i-1]} k=${k[$i-1]} EIGEN failed!!!" >> ${RUNLOG} 2>&1 &
                    done
                done
            done
        done 
    done
fi
